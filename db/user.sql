-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 28 Août 2018 à 10:48
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `nephrobd`
--

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `algorithm` varchar(255) NOT NULL,
  `ce_service` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `email_address` varchar(255) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `is_super_admin` bit(1) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `titre` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `personnel` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_d0ar1h7wcp7ldy6qg5859sol6` (`email_address`),
  UNIQUE KEY `UK_sb8bbouer5wak8vyiiy4pf2bx` (`username`),
  KEY `FKh3685n9pmt5lq3y8kmmtp24mu` (`personnel`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `algorithm`, `ce_service`, `created_at`, `email_address`, `first_name`, `hash`, `is_active`, `is_super_admin`, `last_login`, `last_name`, `salt`, `titre`, `updated_at`, `username`, `personnel`) VALUES
(1, 'sha1', 'admin', '2012-06-18 09:46:58', 'ndiayemor@yahoo.fr', 'admin', '78871d66a0444b180d6f7cc39251672b40b5a59c', b'1', b'1', '2016-04-04 14:01:49', 'mas', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2016-04-04 14:01:49', 'macina', NULL),
(2, 'sha1', 'Directrice', '2012-06-19 05:35:28', 'soeurmadeleine@yahoo.com', 'Soeur', 'cebe06dae4823c978110035945f78f727e4f3f73', b'1', b'1', '2015-09-30 13:13:56', 'Madeleine', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2015-09-30 13:13:56', 'directrice', NULL),
(3, 'sha1', 'Aide Soignante', '2012-06-19 08:59:46', 'mariemadeleine@mas.com', 'NDIONE', '1a8d3eca081af757064eb47f5b71b423052f92c5', b'1', b'0', '2015-05-22 07:38:17', 'Marie Madeleine', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2015-05-22 07:38:17', 'mariemadeleine', NULL),
(4, 'sha1', 'Aide Soignante', '2012-06-22 05:06:47', 'angelique@mas.com', 'Mendy', '1f78349d61e030b83d61001a2a8407dcc7faf037', b'0', b'0', '2015-02-24 08:12:18', 'Angelique', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2015-02-24 14:18:33', 'angelique', NULL),
(5, 'sha1', '25', '2012-06-22 05:07:48', 'mathylde@mas.com', 'Tine', '60924705d0699528b33e10aab602a71aefce56e7', b'1', b'0', '2012-12-12 19:40:05', 'Mathilde', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2012-12-16 19:42:51', 'mathilde', NULL),
(6, 'sha1', 'Assistant Infirmier', '2012-06-22 05:08:59', 'mathias@mas.com', 'Aloys', '2091863c94a4a664472a2221b5823ac736e389a0', b'1', b'0', '2015-09-30 13:04:55', 'Mathias', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2015-09-30 13:04:55', 'mathias', NULL),
(7, 'sha1', 'Aide Soignante', '2012-06-22 05:10:58', 'mariefaye@mas.com', 'Faye', 'b356ce569e51da3794d83d135e6acea07fa3ed82', b'1', b'0', '2015-09-30 07:24:36', 'Marie', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2015-09-30 07:24:36', 'marie', NULL),
(8, 'sha1', 'Aide Soignante', '2012-06-22 05:11:45', 'odile@mas.com', 'Ndiaye', 'c62b6ec6f87132bad5ff0d66749d09a57604fff7', b'1', b'0', '2015-09-29 15:03:25', 'Odile', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2015-09-29 15:03:25', 'odile', NULL),
(9, 'sha1', 'Pediatre', '2012-06-18 09:46:58', 'macina@yahoo.fr', 'Macina', '78871d66a0444b180d6f7cc39251672b40b5a59c', b'1', b'0', '2015-09-30 13:00:22', 'Amadou', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2015-09-30 13:00:22', 'amadou', NULL),
(10, 'sha1', 'Pediatre', '2012-06-19 05:35:28', 'mane@yahoo.com', 'Mane', '2aba23a3a603cca0dcea83aac60f1bd67509c6a3', b'1', b'0', '2015-09-30 12:59:58', 'El Hadji Djibril Gabriel', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2015-09-30 12:59:58', 'gabi', NULL),
(11, 'sha1', 'Medecin', '2012-06-19 08:59:46', 'bernadette@mas.com', 'Bernadette', '1f78349d61e030b83d61001a2a8407dcc7faf037', b'0', b'0', '2015-02-24 07:47:26', 'Soeur', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2015-02-24 14:22:55', 'soeur', NULL),
(12, 'sha1', 'Medecin', '2012-06-22 05:06:47', 'augustin@mas.com', 'Tapsoba', 'a7f638997548b3b9f4516ebc9c6b429cbb34d534', b'1', b'0', '2015-09-30 13:10:31', 'Augustin', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2015-09-30 13:10:31', 'augustin', NULL),
(13, 'sha1', 'Medecin', '2012-06-22 05:07:48', 'talari@mas.com', 'Loompo', 'd004cab35050de9bffc50a717bea00cecfa5c4fb', b'1', b'0', '2014-01-03 11:24:50', 'Paul Talari', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2014-01-03 11:24:50', 'paultalari', NULL),
(14, 'sha1', 'Nutritionniste', '2012-06-22 05:08:59', 'zambruni@yahoo.it', 'Pfaffmann', 'b61b094e86d52ff084b6823d2484173e7e0e17a7', b'1', b'0', '2015-07-30 13:23:36', 'Monica', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2015-07-30 13:23:36', 'monica', NULL),
(15, 'sha1', 'Laborantin', '2012-06-22 05:10:58', 'rosendour@mas.com', 'Ndour', 'f0fa5e51080afde4d4a347f73ff2d8b725b7cee3', b'1', b'0', '2015-09-30 09:28:59', 'Rose', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2015-09-30 09:28:59', 'rose', NULL),
(16, 'sha1', 'Laborantin', '2012-06-22 05:11:45', 'vivianetavarez@mas.com', 'Tavarez', '8594ec4277e347d8b7f7a86580b4e6e4dec2c1f0', b'1', b'0', '2015-09-14 14:47:19', 'Viviane', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2015-09-14 14:47:19', 'viviane', NULL),
(17, 'sha1', 'Sage Femme', '2012-06-19 05:35:28', 'diohmarielouise@yahoo.com', 'Dioh', '1f78349d61e030b83d61001a2a8407dcc7faf037', b'0', b'0', '2015-02-23 11:57:43', 'Marie louise', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2015-02-24 14:21:45', 'marielouise', NULL),
(18, 'sha1', '0', '2012-06-19 08:59:46', 'bernadette2@mas.com', 'Bernadette', 'e2601b2b081f7bf4d260576a0f97e87645db0496', b'1', b'0', '2012-08-10 13:13:20', 'Soeur', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2012-08-10 13:13:20', 'sih17', NULL),
(19, 'sha1', 'Sage Femme', '2012-06-22 05:06:47', 'madamediop@mas.com', 'Diop', '6dbf66b5a6dbc4280313ba56232deccc46317610', b'1', b'0', '2012-08-10 13:15:03', 'Madame', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2012-12-16 21:41:35', 'madame', NULL),
(20, 'sha1', '0', '2012-06-22 05:07:48', 'clemence@mas.com', 'Clemence', 'a6ab5d3e6df7d45681f0b72372f737923a479c96', b'1', b'0', '2015-08-28 12:30:57', 'Soeur', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2015-08-28 12:30:57', 'clemence', NULL),
(21, 'sha1', 'Radiologue', '2012-06-22 05:08:59', 'maimouna@mas.com', 'Diouf', '352c852c133557a9bac8e7a647840e61a059b113', b'1', b'0', '2015-09-29 15:54:55', 'Maimouna', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2015-09-29 15:54:55', 'maimouna', NULL),
(22, 'sha1', 'Nutritionniste', '2012-06-22 05:10:58', 'diabayemaimouna@mas.com', 'Diabaye', 'e9e628edf562bf8cce1b8a172a3c989bc518ed22', b'1', b'0', '2015-09-30 09:56:45', 'Bernadette', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2015-09-30 09:56:45', 'bernadette', NULL),
(23, 'sha1', 'Pharmacienne', '2012-06-22 05:11:45', 'thiawangele@mas.com', 'Thiaw', '23fe35943ad33e3f7bb5d90cf04425ee116d056e', b'1', b'0', '2013-05-14 09:24:21', 'Angele', '9f23052ec2a2f650733ef15ab9435eca', NULL, '2013-05-14 09:24:21', 'angele', NULL),
(24, 'md5', 'Aide Soignante', '2012-12-13 10:52:29', 'hortense@yahoo.fr', 'Sane', '2f8d18753b3989d692be2a639cdc3efa', b'1', b'0', '2012-12-14 08:26:24', 'Hortense', '768db0c7fa66485f05602130d7855848', NULL, '2012-12-16 21:46:49', 'hortense', NULL),
(25, 'md5', 'Pharmacienne', '2012-12-14 09:16:19', 'sophie@mas.com', 'Sarr', '44615c39c65aee1e873ed7efca845272', b'1', b'0', '2015-09-30 11:44:23', 'Sophie', 'ae86cce3ee3a37953f426eea414779b9', NULL, '2015-09-30 11:44:23', 'sophie', NULL),
(26, 'md5', 'Pediatre', '2012-12-19 11:02:04', 'soeur@mas.sn', 'Docteur Soeur', 'b00b133f89e4797c16b0fefedd28d932', b'1', b'0', '2014-01-02 14:38:14', 'madeleine', 'cca498a926a61dfdd69c58a6707863f5', NULL, '2014-01-02 14:38:14', 'matoundour', NULL),
(27, 'md5', 'Pharmacie', '2012-12-27 08:41:33', 'mas@mas.sn', 'Travaly', '8eb6bebf64854f4dec2dec251e0745e8', b'1', b'0', '2013-02-06 11:42:33', 'Anna', 'f8be6ca1d25894beb1a06997eb58b82e', NULL, '2013-02-06 11:42:33', 'anna', NULL),
(28, 'md5', 'statistique', '2013-01-11 16:18:11', 'statistique@saintmartin.com', 'Centre Santé ', '7a1bb1fb62a3d1315bdd3104a53f2ad7', b'1', b'0', '2015-09-30 13:09:55', 'Saint Martin', '1d3b4ca9268a2098f1bd90837d8aee8a', NULL, '2015-09-30 13:09:55', 'statistique', NULL),
(29, 'md5', 'accueil', '2013-03-11 13:45:31', 'bijousamb84@yahoo.fr', 'Samb', '48356ed6653391c44996e878706c27ca', b'1', b'0', '2014-02-28 12:27:57', 'Khady', '47071ee8afad923fa98b5f4c8e530ebb', NULL, '2014-02-28 12:27:57', 'khady', NULL),
(30, 'md5', 'Infirmier', '2013-08-06 12:07:06', 'landry@saintmartin.com', 'Djihounouck', '306399e621d029e3e4b9f167054adf20', b'1', b'0', '2013-11-26 11:53:00', 'landry saturnin', 'ed8477ac5a3e7308171c51f299394363', NULL, '2013-11-26 11:53:00', 'landry', NULL),
(31, 'md5', 'Garde Nutrition', '2013-08-06 15:04:01', 'nutrition@saintmartin.com', 'Personne ', 'b9386470521c2c07df72fdbbef832119', b'1', b'0', '2013-08-07 12:03:33', 'Garde', 'f3a7402ad3804a83a9680c014f7a3283', NULL, '2013-08-07 12:03:33', 'nutrition', NULL),
(32, 'md5', 'infirmier', '2014-01-02 14:09:52', 'mo1984last@hotmail.fr', 'ba', '1879780831d2c83179eb7a43c565864c', b'1', b'0', '2015-09-30 13:04:26', 'mamadou', 'f27814fd5392f1788ab78f4b85e789bb', NULL, '2015-09-30 13:04:26', 'mamadou ', NULL),
(33, 'md5', 'Sage Femme', '2014-04-10 13:20:43', 'binger@stmartin.com', 'gning', '1b64bf64d74e8608f0a61536081c753c', b'1', b'0', '2015-09-30 13:02:15', 'Marguerite Binger', 'b0c3ec7c8cb387cffdc6a227176b1937', NULL, '2015-09-30 13:02:15', 'marguerite', NULL),
(34, 'md5', 'nutritioniste', '2014-06-30 15:43:10', 'amelisse27@yahoo.fr', 'nankeu', 'ec57345e4af372176956f495453b303c', b'1', b'0', '2014-06-30 15:49:20', 'liliane', '85779f8643a69b2cfe29ec19cbcc2bef', NULL, '2014-06-30 15:49:20', 'liliane', NULL),
(35, 'md5', 'Accueil', '2015-01-15 14:04:26', 'mariannelaye1@gmail.com', 'Mbaye', 'eaabf930b16d217cd62d00c866fae1e1', b'1', b'0', '2015-09-30 07:02:43', 'Marianne', '6ceb1809afc730f90d47b778a9620ad7', NULL, '2015-09-30 07:02:43', 'marianne', NULL),
(36, 'md5', 'Accueil', '2015-01-15 14:07:47', 'gersindiaye@gmail.com', 'Ndiaye', '808066d79980933a8e9957f817e1d867', b'1', b'0', '2015-09-30 07:19:25', 'Germaine', '28b1b2d25dae847abc40dcacbc410279', NULL, '2015-09-30 07:19:25', 'germaine', NULL),
(37, 'md5', 'Triage', '2015-01-15 14:12:21', 'gueyemareme10@hotmail.fr', 'Gaye', '1946e0a5e25f4db3672c22c6ee5f0179', b'1', b'0', '2015-01-15 14:12:43', 'Marieme', '71a17fe345dc2d23ba163a488d44d027', NULL, '2015-01-15 14:12:43', 'maremegueye', NULL),
(38, 'md5', 'pharmacie', '2015-01-15 16:28:50', 'guy@gmail.com', 'sambou', '205db9ad4046cc9b2c3938f47df3f272', b'1', b'0', '2015-09-30 12:35:28', 'guy', '575c4110880ce02ece909485ff2558f8', NULL, '2015-09-30 12:35:28', 'guy', NULL),
(39, 'md5', 'Medecin', '2015-02-24 14:24:22', 'awa@gmail.com', 'Badiane', 'f2f2c27039a88905458d0dd68cd4eec0', b'1', b'0', '2015-05-28 08:18:46', 'Awa', 'd38f8ddc4a3bc573adbe131e4ea07844', NULL, '2015-05-28 08:18:46', 'awa', NULL),
(40, 'md5', 'Nutritionniste', '2015-06-03 12:25:43', 'tabithakieviet@gmail.com', 'Kieviet', '82967534ebf2001eb7660559824c3db7', b'1', b'0', '2015-07-28 14:06:53', 'Tabitha', '1e967f394e74bc1ecc4e3b63e6362ea4', NULL, '2015-07-28 14:06:53', 'tabitha', NULL),
(43, 'sha', 'info', '2018-03-09 00:34:46', 'updo@mas.sn', 'Diagne', '3accfa6f7a0c387a97aee8357c114eeb8dbda5ba5b505011930e79c3600d3955', b'1', b'1', NULL, 'Abdou', '261cd56e7b8c24ac163337abae597d77ca45207611700bbe020000e9b1c48558', NULL, NULL, 'updo', NULL),
(44, 'sha', 'informatique', '2018-03-18 07:50:39', 'infirmier@gmail.com', 'infirmier', '94f90f5119b251544ef2d9a220d7e4b27e1656a06e0de9c8bbc37262b581dfd3', b'1', b'0', NULL, 'infirmier', '776b7f5a10ebc4b11d031e64792fbdffd22f1eba61eccc80099955888242ecbb', NULL, NULL, 'infirmier', NULL);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FKdkbbmtclilr3kofh5cq04hviy` FOREIGN KEY (`personnel`) REFERENCES `personnelmedical` (`id`),
  ADD CONSTRAINT `FKh3685n9pmt5lq3y8kmmtp24mu` FOREIGN KEY (`personnel`) REFERENCES `personnelmedical` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
