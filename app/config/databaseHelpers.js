export const errorCB = err => {
  console.log("SQL Error: " + err);
};
export const errorInsert = err => {
  console.log("Database populating error: " + err);
};
export const errorOpen = err => {
  console.log("Database opening error: " + err);
};
export const successCB = () => {
  console.log("SQL executed fine");
};

export const openCB = () => {
  console.log("Database OPENED");
};

export const successCreate = () => {
  console.log("Table successfully created");
};

export const errorCreate = err => {
  console.log("Error while creating table :" + err);
};

export const successInsert = () => {
  console.log("Data successfully inserted!");
};
