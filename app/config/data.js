export const patients = [
  {
    code: "1",
    prenom: "Maba",
    nom: "Diakhoumpa",
    sexe: "Homme",
    ddn: "05/06/1965",
    ldn: "Dakar",
    picture: {
      large: "https://randomuser.me/api/portraits/men/2.jpg",
      medium: "https://randomuser.me/api/portraits/med/men/2.jpg",
      thumbnail: "https://randomuser.me/api/portraits/thumb/men/2.jpg"
    }
  },
  {
    code: "2",
    prenom: "Mana",
    nom: "Fall",
    sexe: "Femme",
    ddn: "19/09/1998",
    ldn: "Nouakchott",
    picture: {
      large: "https://randomuser.me/api/portraits/women/1.jpg",
      medium: "https://randomuser.me/api/portraits/med/women/1.jpg",
      thumbnail: "https://randomuser.me/api/portraits/thumb/women/1.jpg"
    }
  },
  {
    code: "3",
    prenom: "Djibril",
    nom: "Diagne",
    sexe: "Homme",
    ddn: "04/09/1996",
    ldn: "Dakar",
    picture: {
      large: "https://randomuser.me/api/portraits/men/1.jpg",
      medium: "https://randomuser.me/api/portraits/med/men/1.jpg",
      thumbnail: "https://randomuser.me/api/portraits/thumb/men/1.jpg"
    }
  },
  {
    code: "4",
    prenom: "Sidy",
    nom: "Diop",
    sexe: "Homme",
    ddn: "14/11/1985",
    ldn: "Thies",
    picture: {
      large: "https://randomuser.me/api/portraits/men/3.jpg",
      medium: "https://randomuser.me/api/portraits/med/men/3.jpg",
      thumbnail: "https://randomuser.me/api/portraits/thumb/men/3.jpg"
    }
  },
  {
    code: "5",
    prenom: "Fatoumata",
    nom: "Ba",
    sexe: "Femme",
    ddn: "16/07/1999",
    ldn: "Dakar",
    picture: {
      large: "https://randomuser.me/api/portraits/women/2.jpg",
      medium: "https://randomuser.me/api/portraits/med/women/2.jpg",
      thumbnail: "https://randomuser.me/api/portraits/thumb/women/2.jpg"
    }
  },
  {
    code: "6",
    prenom: "Nandite",
    nom: "Fall",
    sexe: "Homme",
    ddn: "01/01/2000",
    ldn: "Shangai",
    picture: {
      large: "https://randomuser.me/api/portraits/men/4.jpg",
      medium: "https://randomuser.me/api/portraits/med/men/4.jpg",
      thumbnail: "https://randomuser.me/api/portraits/thumb/men/4.jpg"
    }
  }
];

export const rendezvous = [
  {
    id: 1,
    daterv: "2018-11-03T09:30:00",
    codepatient: "2"
  },
  {
    id: 1,
    daterv: "2018-09-13T14:25:00",
    codepatient: "1"
  },
  {
    id: 1,
    daterv: "2018-09-04T10:00:00",
    codepatient: "4"
  },
  {
    id: 1,
    daterv: "2018-10-12T08:30:00",
    codepatient: "3"
  }
];
