import { Dimensions } from "react-native";
const blue = "#60b0f4";
export default {
  color: blue,
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  header: {
    backgroundColor: blue
  },
  headerTitle: {
    color: "white"
  },
  button: {
    marginVertical: 5,
    backgroundColor: blue,
    width: 300,
    height: 45,
    borderColor: "transparent",
    borderWidth: 0,
    borderRadius: 5
  },
  menuLogo: {
    width: 300,
    height: 300
  },
  searchInput: {
    padding: 10,
    borderColor: "#CCC",
    borderWidth: 1
  },
  logoContainer: {
    flex: 2,
    alignItems: "center",
    justifyContent: "center"
  },
  gridView: {
    paddingTop: 5,
    flex: 4
  },
  gridRow: {
    flexDirection: "row",
    margin: 5,
    justifyContent: "space-around"
  },
  preview: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
    height: Dimensions.get("window").height,
    width: Dimensions.get("window").width
  },
  capture: {
    flex: 0,
    backgroundColor: "#fff",
    borderRadius: 5,
    color: "#000",
    padding: 10,
    margin: 40
  }
};
