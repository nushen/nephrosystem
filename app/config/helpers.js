import axios from "axios";
import { serverBaseUrl } from "./settings";
var SQLite = require("react-native-sqlite-storage");
export const titleCase = str => {
  return str
    .toLowerCase()
    .split(" ")
    .map(function(word) {
      return word.replace(word[0], word[0].toUpperCase());
    })
    .join(" ");
};

export const getPatients = id => {
  return axios
    .get(serverBaseUrl + "users/" + id + "/patients")
    .then(response => {
      return response.data.data;
    })
    .catch(error => {
      console.log("---- ERREUR ------- :" + error);
      return error.data;
    });
};

//Récupérer les fichiers d'un patient
export const getFichiersPatient = id => {
  return axios
    .get(serverBaseUrl + "patients/" + id + "/fichiers")
    .then(response => {
      return response.data.data;
    })
    .catch(error => {
      console.log("---- ERREUR ------- :" + error);
      return error.data;
    });
};

export const uploadFichier = async fichier => {
  return axios
    .post(serverBaseUrl + "fichiers", { ...fichier })
    .then(response => {
      return response.data.data;
    })
    .catch(error => {
      console.log("---- ERREUR ------- :" + error);
      return error.data;
    });
};

//Récupérer les fichiers d'un patient
export const getFichiers = () => {
  return axios
    .get(serverBaseUrl + "fichiers")
    .then(response => {
      return response.data.data;
    })
    .catch(error => {
      console.log("---- ERREUR ------- :" + error);
      return error.data;
    });
};

//Récupérer les rendez-vous du médecin connecté
export const getRendezVous = id => {
  return axios
    .get(serverBaseUrl + "users/" + id + "/rendezVous")
    .then(response => {
      return response.data.data;
    })
    .catch(error => {
      console.log("---- ERREUR ------- :" + error);
      return error.data;
    });
};

//Récupérer le patient à partir de son id
export const getPatient = id => {
  return axios
    .get(serverBaseUrl + "patients/" + id)
    .then(response => {
      return response.data.data;
    })
    .catch(error => {
      console.log("---- ERREUR ------- :" + error);
      return error.data;
    });
};

export const sync = () => {
  //Synchronize local database and server database
};
