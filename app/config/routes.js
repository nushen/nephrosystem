import { createStackNavigator } from "react-navigation";
import React from "react";
import { View, Text } from "react-native";
import { Icon } from "react-native-elements";
import Patients from "../screens/patients/Patients";
import PatientDetails from "../screens/patients/PatientDetails";
import RendezVous from "../screens/rendezvous/RendezVous";
import RendezVousDetails from "../screens/rendezvous/RendezVousDetails";
import Home from "../screens/Home";
import Login from "../screens/login/Login";
import Parametres from "../screens/parametres/Parametres";
import Sync from "../screens/parametres/Sync";
import Fichiers from "../screens/fichiers/Fichiers";
import PatientFolder from "../screens/fichiers/PatientFolder";
import AddPhoto from "../screens/fichiers/AddPhoto";
import DisplayFile from "../screens/fichiers/DisplayFile";

import styles from "../config/styles";

export const PatientsStack = createStackNavigator({
  Patients: {
    screen: Patients,
    navigationOptions: {
      title: "Patients",
      header: null
    }
  },
  PatientDetails: {
    screen: PatientDetails,
    navigationOptions: ({ navigation }) => ({
      title: `${navigation.state.params.nom.toUpperCase()} ${navigation.state.params.prenom.toUpperCase()}`
    })
  }
});

export const RendezVousStack = createStackNavigator({
  RendezVous: {
    screen: RendezVous,
    navigationOptions: {
      title: "Mes Rendez-Vous",
      header: null
    }
  },
  RendezVousDetails: {
    screen: RendezVousDetails,
    navigationOptions: ({ navigation }) => ({
      title: `${navigation.state.params.title.toUpperCase()}`
    })
  }
});

export const ParametresStack = createStackNavigator({
  Parametres: {
    screen: Parametres,
    navigationOptions: {
      title: "Mes Rendez-Vous",
      header: null
    }
  },
  Sync: {
    screen: Sync,
    navigationOptions: () => ({
      title: "Synchronisation"
    })
  }
});

export const FichiersStack = createStackNavigator({
  Fichiers: {
    screen: Fichiers,
    navigationOptions: {
      title: "Fichiers",
      header: null
    }
  },
  PatientFolder: {
    screen: PatientFolder,
    navigationOptions: ({ navigation }) => ({
      title: `${navigation.state.params.nom.toUpperCase()} ${navigation.state.params.prenom.toUpperCase()}`
    })
  },
  AddPhoto: {
    screen: AddPhoto,
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  },
  DisplayFile: {
    screen: DisplayFile,
    navigationOptions: ({ navigation }) => ({
      title: `${navigation.state.params.titre}`
    })
  }
});

export const LoginStack = loggedIn => {
  return createStackNavigator(
    {
      Login: {
        screen: Login,
        navigationOptions: {
          header: null
        }
      },
      Home: {
        screen: HomeStack,
        navigationOptions: {
          header: null
        }
      }
    },
    {
      initialRouteName: loggedIn ? "Home" : "Login"
    }
  );
};

export const HomeStack = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        header: null
      }
    },
    Patients: {
      screen: PatientsStack,
      navigationOptions: {
        title: "Patients"
      }
    },
    RendezVous: {
      screen: RendezVousStack,
      navigationOptions: {
        title: "Rendez-vous"
      }
    },
    Fichiers: {
      screen: FichiersStack,
      navigationOptions: {
        title: "Fichiers"
      }
    },
    Parametres: {
      screen: ParametresStack,
      navigationOptions: {
        title: "Paramètres"
      }
    }
  },
  {
    initialRouteName: "Home",
    navigationOptions: {
      headerStyle: styles.header,
      headerTitleStyle: styles.headerTitle,
      headerTintColor: "white"
    }
  }
);
