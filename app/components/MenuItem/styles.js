import globalStyles from "../../config/styles";
import { Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");
export default {
  itemContainer: {
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
    backgroundColor: globalStyles.color,
    padding: 10,
    width: width * 0.3,
    height: height * 0.2,
    margin: 10
  },
  itemIcon: {
    width: 50,
    height: 50
  },
  itemLabel: {
    fontWeight: "600",
    fontSize: 16,
    color: "#fff"
  }
};
