import React from "react";
import { View, Text, Image } from "react-native";
import styles from "./styles";
import globalStyles from "../../config/styles";

const MenuItem = props => {
  return (
    <View style={styles.itemContainer}>
      <Image source={props.image} alt={props.title} style={styles.itemIcon} />
      <Text style={styles.itemLabel}>{props.title}</Text>
    </View>
  );
};
export default MenuItem;
