import globalStyles from "../../config/styles";
export default {
  title: {
    fontWeight: "bold",
    color: globalStyles.color
  }
};
