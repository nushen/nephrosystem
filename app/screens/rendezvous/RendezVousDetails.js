import React, { Component } from "react";
import { ScrollView, View } from "react-native";
import { Tile, ListItem, Button } from "react-native-elements";
import { rvBackground } from "../../config/images";
import styles from "../../config/styles";
import moment from "moment";
import { getPatient } from "./helpers";
import localStyles from "./styles";
var SQLite = require("react-native-sqlite-storage");
import * as dbHelpers from "../../config/databaseHelpers.js";

class PatientDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      patient: {},
      isButtonLoading: false,
      error: ""
    };
  }
  date = daterv => {
    return moment(daterv).format("DD/MM/YYYY");
  };
  time = daterv => {
    return moment(daterv).format("HH:mm");
  };
  goToPatientDetails = async patient => {
    this.setState({ isButtonLoading: true });
    var db = SQLite.openDatabase(
      {
        name: "nephrosystem.db"
      },
      dbHelpers.openCB,
      dbHelpers.errorOpen
    );
    await db.transaction(
      tx => {
        tx.executeSql("select * from patients", [], (tx, results) => {
          console.log("Query completed");
          var len = results.rows.length;
          let rows = results.rows.raw();
          console.log(rows);

          if (len > 0)
            this.setState({
              patient: rows.find(
                row => row.id == this.props.navigation.state.params.patient_id
              ),
              isButtonLoading: false
            });
          else
            this.setState({
              error: "Patient introuvable. Synchronisez svp et réessayez!",
              isButtonLoading: false
            });
        });
      },
      () => {
        dbHelpers.successCB();
        this.props.navigation.navigate("PatientDetails", {
          ...this.state.patient
        });
      },
      error => {
        dbHelpers.errorCB(error);
        if (this.state.error == "")
          this.props.navigation.navigate("PatientDetails", {
            ...this.state.patient
          });
        else alert(this.state.error);
      }
    );
  };

  render() {
    const {
      id,
      title,
      daterv,
      datedebut,
      datefin,
      description,
      patient_id
    } = this.props.navigation.state.params;

    return (
      <ScrollView>
        <Tile
          imageSrc={{ uri: rvBackground }}
          featured
          title={title}
          caption={this.date(daterv)}
        />
        <View style={styles.container}>
          <Button
            onPress={() => this.goToPatientDetails(patient_id)}
            loading={this.state.isButtonLoading}
            titleStyle={{ fontWeight: "700" }}
            loadingProps={{ size: "large", color: "white" }}
            activityIndicatorStyle={{ backgroundColor: "white" }}
            title="Voir patient"
            buttonStyle={styles.button}
          />
        </View>
        <View>
          <ListItem
            title="Titre"
            titleStyle={localStyles.title}
            rightTitle={title}
            hideChevron
          />
          <ListItem
            title="Description"
            titleStyle={localStyles.title}
            rightTitle={description}
            hideChevron
          />
        </View>

        <View>
          <ListItem
            title="Date"
            titleStyle={localStyles.title}
            rightTitle={this.date(daterv)}
            hideChevron
          />
          <ListItem
            title="De"
            titleStyle={localStyles.title}
            rightTitle={this.time(datedebut)}
            hideChevron
          />
          <ListItem
            title="À"
            titleStyle={localStyles.title}
            rightTitle={this.time(datefin)}
            hideChevron
          />
        </View>
      </ScrollView>
    );
  }
}

export default PatientDetails;
