import axios from "axios";
import { serverBaseUrl } from "../../config/settings";

//Récupérer les rendez-vous du médecin connecté
export const getRendezVous = id => {
  return axios
    .get(serverBaseUrl + "users/" + id + "/rendezVous")
    .then(response => {
      return response.data.data;
    })
    .catch(error => {
      console.log("---- ERREUR ------- :" + error);
      return error.data;
    });
};

//Récupérer le patient à partir de son id
export const getPatient = id => {
  return axios
    .get(serverBaseUrl + "patients/" + id)
    .then(response => {
      return response.data.data;
    })
    .catch(error => {
      console.log("---- ERREUR ------- :" + error);
      return error.data;
    });
};
