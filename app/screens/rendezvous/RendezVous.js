import React from "react";
import {
  View,
  Text,
  ScrollView,
  ActivityIndicator,
  AsyncStorage
} from "react-native";
import styles from "../../config/styles";
import { ListItem } from "react-native-elements";
import SearchInput, { createFilter } from "react-native-search-filter";
import * as Helpers from "./helpers";
import moment from "moment";
var SQLite = require("react-native-sqlite-storage");
import * as dbHelpers from "../../config/databaseHelpers.js";
const KEYS_TO_FILTERS = ["title", "daterv"];

export default class RendezVous extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      rendezvous: [],
      searchTerm: "",
      error: ""
    };
  }

  goToDetails = rv => {
    this.props.navigation.navigate("RendezVousDetails", { ...rv });
  };

  executeQuery = (query, parameters) => {
    var db = SQLite.openDatabase(
      {
        name: "nephrosystem.db"
      },
      dbHelpers.openCB,
      dbHelpers.errorOpen
    );
    db.transaction(
      tx => {
        tx.executeSql(query, parameters, (tx, results) => {
          console.log("Query completed");
          var len = results.rows.length;
          let rows = results.rows.raw();
          if (len > 0) this.setState({ rendezvous: rows, loading: false });
          else
            this.setState({
              error: "Aucun rendez-vous. Synchronisez svp et réessayez!",
              loading: false
            });
        });
      },
      dbHelpers.successCB,
      error => {
        dbHelpers.errorCB(error);
      }
    );
  };

  date = daterv => {
    return moment(daterv).format("DD/MM/YYYY");
  };

  time = daterv => {
    return moment(daterv).format("HH:mm");
  };

  componentDidMount = async () => {
    const user = JSON.parse(await AsyncStorage.getItem("user"));
    await this.executeQuery("select * from rendezvous", []);
    /*
    try {
      const rendezvous = await Helpers.getRendezVous(user.id);
      if (rendezvous) {
        rendezvous.length > 0
          ? this.setState({ rendezvous: rendezvous })
          : this.setState({ error: "Vous n'avez aucun rendez-vous à venir.'" });
      } else {
        this.setState({
          error: "Erreur serveur. Veuillez réessayer plus tard"
        });
      }
    } catch (error) {
      console.log(error);

      this.setState({
        error: "Serveur indisponible. Veuillez réessayer plus tard"
      });
    }
    this.setState({ loading: false });*/
  };

  render() {
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color={styles.color} />
        </View>
      );
    } else {
      if (this.state.error != "") {
        return (
          <View style={styles.container}>
            <Text style={{ fontSize: 20, color: styles.color }}>
              {this.state.error}
            </Text>
          </View>
        );
      }
    }
    const filteredRendezVous = this.state.rendezvous.filter(
      createFilter(this.state.searchTerm, KEYS_TO_FILTERS)
    );
    return (
      <View>
        <SearchInput
          onChangeText={term => {
            this.setState({ searchTerm: term });
          }}
          style={styles.searchInput}
          placeholder="Rechercher un rendez-vous"
        />
        <ScrollView>
          <View containerStyle={{ marginTop: 0 }}>
            {filteredRendezVous.map(rv => {
              return (
                <ListItem
                  key={rv.id}
                  hideChevron
                  title={rv.title}
                  rightTitleNumberOfLines={2}
                  rightTitle={`${this.date(rv.datedebut)} \n ${this.time(
                    rv.datedebut
                  )}`}
                  rightTitleStyle={{ color: styles.color }}
                  subtitle={rv.description}
                  onPress={() => this.goToDetails(rv)}
                />
              );
            })}
          </View>
        </ScrollView>
      </View>
    );
  }
}
