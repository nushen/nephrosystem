import axios from "axios";
import { serverBaseUrl } from "../../config/settings";
var SQLite = require("react-native-sqlite-storage");

export const getNombreFichiersPatient = id => {
  return axios
    .get(serverBaseUrl + "patients/" + id + "/fichiers/count")
    .then(response => {
      return response.data.data;
    })
    .catch(error => {
      console.log("---- ERREUR ------- :" + error);
      return error.data;
    });
};

export const getFichiersPatient = id => {
  return axios
    .get(serverBaseUrl + "patients/" + id + "/fichiers")
    .then(response => {
      return response.data.data;
    })
    .catch(error => {
      console.log("---- ERREUR ------- :" + error);
      return error.data;
    });
};

export const addPhotoToDatabase = async (titre, source, type, patient) => {
  var db = SQLite.openDatabase(
    {
      name: "nephrosystem.db"
    },
    dbHelpers.openCB,
    dbHelpers.errorOpen
  );
  db.transaction(
    tx.executeSql(
      "insert into fichiers (titre, source, type, patient, synced) values (?, ?, ?, ?, ?)",
      [titre, source, type, patient, false],
      (tx, results) => {
        console.log("Query completed");
        var len = results.rows.length;
        let rows = results.rows.raw();
        console.log("Query succesfully executed : fichiers table insertion");
      }
    )
  );
};
