import React, { Component } from "react";
import { View, Text, Dimensions, TouchableOpacity } from "react-native";
import { Icon } from "react-native-elements";
import styles from "../../config/styles";
import { RNCamera } from "react-native-camera";
import { cloudinaryConfig } from "../../config/settings";
import axios from "axios";
import * as dbHelpers from "../../config/databaseHelpers";
import { addPhotoToDatabase } from "./helpers";
import moment from "moment";
var RNFS = require("react-native-fs");
var SQLite = require("react-native-sqlite-storage");

export default class AddPhoto extends Component {
  savePictureUriInServer = async (codepatient, dir, timestamp) => {
    //Save picture uri to server
    //Cloudinary config
  };

  savePictureLocally = async uri => {
    let patient = this.props.navigation.state.params;
    let codepatient = patient.codepatient;
    let timestamp = ((Date.now() / 1000) | 0).toString();
    let dir = (await RNFS.ExternalDirectoryPath) + "/" + codepatient;
    await RNFS.mkdir(dir)
      .then(result => console.log("Directory created!"))
      .catch(error => console.log(error));
    //test if the dir has been created or not
    await RNFS.exists(dir)
      .then(result => console.log(result))
      .catch(error => console.log(error));

    //Move file from cache to phone storage
    await RNFS.moveFile(
      uri.substr(7), //remove "file://" prefix
      dir + "/" + codepatient + "_" + timestamp + ".jpg"
    )
      .then(() => {
        console.log("File moved!");
      })
      .catch(error => {
        console.log(error);
        alert("Erreur lors de la prise de photo!");
      });

    await this.savePictureUriInServer(codepatient, dir, timestamp);

    //Add photo to server
    let titre = moment(Date.now())
      .format("DD/MM/YYYY HH:mm")
      .toString();
    let source = "file://" + dir + "/" + codepatient + "_" + timestamp + ".jpg";
    //await addPhotoToDatabase(titre, source, "image", patient.id);
    var db = SQLite.openDatabase(
      {
        name: "nephrosystem.db"
      },
      dbHelpers.openCB,
      dbHelpers.errorOpen
    );
    db.transaction(
      tx => {
        tx.executeSql(
          "insert into fichiers (titre, source, type, patient, synced) values (?, ?, ?, ?, ?)",
          [titre, source, "image", patient.id, false],
          (tx, results) => {
            console.log("Query completed");
            var len = results.rows.length;
            let rows = results.rows.raw();
            console.log(
              "Query succesfully executed : fichiers table insertion"
            );
          }
        );
      },
      () => {
        dbHelpers.successCB();
        this.props.navigation.navigate("DisplayFile", {
          patient,
          source,
          titre
        });
      },
      () => {
        dbHelpers.errorCB();
        this.props.navigation.navigate("DisplayFile", {
          patient,
          source,
          titre
        });
      }
    );
  };

  takePicture = async () => {
    try {
      const options = { quality: 0.5, base64: true };
      const data = await this.camera.takePictureAsync(options);
      console.log(data.uri);
      await this.savePictureLocally(data.uri);
      //await this.upload(data);
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          autoFocus={RNCamera.Constants.AutoFocus.on}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.auto}
          permissionDialogTitle={"Permission pour utiliser votre camera"}
          permissionDialogMessage={
            "Nous avons besoin de votre permission pour utiliser votre camera"
          }
        >
          <TouchableOpacity
            style={{ position: "absolute", left: 15, top: 10, zIndex: 100 }}
            onPress={() =>
              this.props.navigation.navigate("PatientFolder", {
                ...this.props.navigation.state.params
              })
            }
          >
            <Icon
              name="angle-left"
              size={30}
              color={styles.color}
              type="font-awesome"
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{ marginBottom: 20 }}
            onPress={this.takePicture.bind(this)}
          >
            <Icon
              name="circle-outline"
              size={100}
              color={styles.color}
              type="material-community"
            />
          </TouchableOpacity>
        </RNCamera>
      </View>
    );
  }
}
