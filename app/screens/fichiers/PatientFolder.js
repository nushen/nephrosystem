import React, { Component } from "react";
import { ScrollView, View, Text, ActivityIndicator, Image } from "react-native";
import {
  Tile,
  List,
  ListItem,
  Button,
  Icon,
  Card
} from "react-native-elements";
import localStyles from "./styles";
import styles from "../../config/styles";
import * as Helpers from "./helpers";
import * as dbHelpers from "../../config/databaseHelpers";
var SQLite = require("react-native-sqlite-storage");
import SearchInput, { createFilter } from "react-native-search-filter";
const KEYS_TO_FILTERS = ["titre"];

class PatientFolder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      fichiers: [],
      searchTerm: "",
      error: ""
    };
  }
  goToAddPhoto() {
    const patient = this.props.navigation.state.params;
    this.props.navigation.navigate("AddPhoto", { ...patient });
  }
  display = (titre, source) => {
    const patient = this.props.navigation.state.params;
    this.props.navigation.navigate("DisplayFile", {
      patient,
      source,
      titre
    });
  };

  executeQuery = (query, parameters) => {
    var db = SQLite.openDatabase(
      {
        name: "nephrosystem.db"
      },
      dbHelpers.openCB,
      dbHelpers.errorOpen
    );
    db.transaction(
      tx => {
        tx.executeSql(query, parameters, (tx, results) => {
          console.log("Query completed");
          var len = results.rows.length;
          let rows = results.rows.raw();

          if (len > 0) {
            const patientFichiers = rows.filter(
              row => row.patient == this.props.navigation.state.params.id
            );

            this.setState({ fichiers: patientFichiers, loading: false });
          } else
            this.setState({
              error: "Ce patient n'a aucun fichier",
              loading: false
            });
        });
      },
      dbHelpers.successCB,
      error => {
        dbHelpers.errorCB(error);
      }
    );
  };

  componentDidMount = async () => {
    await this.executeQuery("select * from fichiers", []);
  };

  render() {
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color={styles.color} />
        </View>
      );
    } else {
      if (this.state.fichiers.length == 0) {
        return (
          <View style={styles.container}>
            <Text style={{ fontSize: 20, color: styles.color }}>
              {this.state.error}
            </Text>
            <Button
              title="Nouvelle photo"
              icon={
                <Icon
                  name="camera-enhance"
                  type="material_community"
                  size={25}
                  color="white"
                />
              }
              buttonStyle={styles.button}
              onPress={() => this.goToAddPhoto()}
            />
          </View>
        );
      }
    }
    const filteredFichiers = this.state.fichiers.filter(
      createFilter(this.state.searchTerm, KEYS_TO_FILTERS)
    );
    return (
      <View>
        <View>
          <SearchInput
            onChangeText={term => {
              this.setState({ searchTerm: term });
            }}
            style={styles.searchInput}
            placeholder="Rechercher un fichier"
          />
          <ScrollView containerStyle={{ marginTop: 0 }}>
            <View style={styles.container}>
              <Button
                title="Nouvelle photo"
                icon={
                  <Icon
                    name="camera-enhance"
                    type="material_community"
                    size={25}
                    color="white"
                  />
                }
                buttonStyle={styles.button}
                onPress={() => this.goToAddPhoto()}
              />
            </View>
            <View>
              {filteredFichiers.map((fichier, index) => {
                console.log(fichier);
                return (
                  <ListItem
                    key={index}
                    leftIcon={
                      fichier.type.localeCompare("image") == 0
                        ? { name: "photo", color: styles.color }
                        : { name: "videocam", color: styles.color }
                    }
                    title={`${fichier.titre}`}
                    subtitle={`${fichier.type}`}
                    rightAvatar={{ source: { uri: fichier.source } }}
                    onPress={() =>
                      this.display(fichier.titre, fichier.source, fichier.type)
                    }
                  />
                );
              })}
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default PatientFolder;
