import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import styles from "../../config/styles";

export default class DisplayFile extends Component {
  render() {
    const uri = this.props.navigation.state.params.source;
    return (
      <View style={styles.container}>
        <Image style={styles.preview} source={{ uri }} alt="Displayed image" />
      </View>
    );
  }
}
