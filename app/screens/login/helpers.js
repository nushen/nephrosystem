import { serverBaseUrl } from "../../config/settings";
import axios from "axios";
import sha1 from "sha1";

export const login = async (login, password) => {
  //attempt to login to api
  const users = await getAllUsers();
  let found = null;
  found = users.find(user => {
    return user.username.localeCompare(login) == 0;
  });
  if (found && found.hash.localeCompare(sha1(found.salt + password)) == 0) {
    return found;
  }
  return "error";
};

getAllUsers = () => {
  return axios
    .get(serverBaseUrl + "users")
    .then(response => {
      return response.data.data;
    })
    .catch(error => {
      console.log("---- ERREUR ------- :" + error);
      return error.data;
    });
};
