import { Dimensions } from "react-native";
export default {
  formContainer: {
    flex: 1,
    alignItems: "center",
    padding: 10,
    marginBottom: 20,
    justifyContent: "space-between"
  },
  formElement: {
    width: Dimensions.get("screen").width - 10
  }
};
