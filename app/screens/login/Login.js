import React from "react";
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import {
  //FormLabel,
  //FormInput,
  Input,
  Icon,
  Button
  //FormValidationMessage
} from "react-native-elements";
import styles from "../../config/styles";
import localStyles from "./styles";
import { logo } from "../../config/images";
import * as Helpers from "./helpers";

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      username: "",
      password: "",
      isLoading: false
    };
  }
  connecter = async () => {
    this.setState({ error: false, isLoading: true });
    const username = this.state.username;
    const password = this.state.password;
    try {
      const tryLogin = await Helpers.login(username, password);
      if (tryLogin == "error") {
        this.setState({ error: true });
      } else {
        await AsyncStorage.setItem("user", JSON.stringify(tryLogin));
        this.props.navigation.navigate("Home");
        console.log(tryLogin);
      }
    } catch (error) {
      alert("Erreur serveur. Réessayez plus tard.");
    }
    this.setState({ isLoading: false });
  };

  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center" }}>
        <View style={styles.logoContainer}>
          <Image source={logo} style={styles.menuLogo} />
        </View>
        <View style={localStyles.formContainer}>
          <Input
            label="Username"
            labelStyle={{ color: styles.color, fontSize: 18 }}
            inputStyle={localStyles.formElement}
            placeholder="john"
            leftIcon={<Icon name="person" size={24} color={styles.color} />}
            shake={this.state.error}
            errorStyle={{ color: "red" }}
            onChangeText={login => {
              this.setState({ username: login });
            }}
          />
          <Input
            label="Password"
            labelStyle={{ color: styles.color, fontSize: 18 }}
            inputStyle={localStyles.formElement}
            placeholder="********"
            leftIcon={<Icon name="lock" size={24} color={styles.color} />}
            shake={this.state.error}
            errorMessage={
              this.state.error
                ? "Nom d'utilisateur ou mot de passe incorrect"
                : " "
            }
            errorStyle={{ color: "red" }}
            onChangeText={pwd => this.setState({ password: pwd })}
            secureTextEntry
          />
          <Button
            buttonStyle={localStyles.formElement}
            onPress={() => this.connecter()}
            loading={this.state.isLoading}
            titleStyle={{ fontWeight: "700" }}
            loadingProps={{ size: "large", color: "white" }}
            activityIndicatorStyle={{ backgroundColor: "green" }}
            title="Valider"
            buttonStyle={styles.button}
          />
        </View>
      </View>
    );
  }
}
