import React from "react";
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import { Icon } from "react-native-elements";
import styles from "../config/styles";
import { MenuItem } from "../components/MenuItem";
import {
  logo,
  patientsIcon,
  parametresIcon,
  fichiersIcon,
  rendezVousIcon
} from "../config/images";

export default class Home extends React.Component {
  constructor(props) {
    super(props);
  }
  _goTo = screen => {
    this.props.navigation.navigate(screen);
  };
  logout = async () => {
    await AsyncStorage.removeItem("user");
    this.props.navigation.navigate("Login");
  };
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Image source={logo} style={styles.menuLogo} />
        </View>
        <View style={styles.gridView}>
          <View style={styles.gridRow}>
            <TouchableOpacity onPress={() => this._goTo("Patients")}>
              <MenuItem image={patientsIcon} title="Patients" />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._goTo("RendezVous")}>
              <MenuItem image={rendezVousIcon} title="Rendez-Vous" />
            </TouchableOpacity>
          </View>
          <View style={styles.gridRow}>
            <TouchableOpacity onPress={() => this._goTo("Fichiers")}>
              <MenuItem image={fichiersIcon} title="Fichiers" />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._goTo("Parametres")}>
              <MenuItem image={parametresIcon} title="Parametres" />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
          <Icon name="exit-to-app" color="red" onPress={() => this.logout()} />
        </View>
      </View>
    );
  }
}
