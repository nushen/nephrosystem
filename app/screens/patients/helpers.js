import axios from "axios";
import { serverBaseUrl } from "../../config/settings";

export const getPatients = id => {
  return axios
    .get(serverBaseUrl + "users/" + id + "/patients")
    .then(response => {
      return response.data.data;
    })
    .catch(error => {
      console.log("---- ERREUR ------- :" + error);
      return error.data;
    });
};
