import React from "react";
import {
  View,
  Text,
  ScrollView,
  AsyncStorage,
  ActivityIndicator
} from "react-native";
import styles from "../../config/styles";
import SearchInput, { createFilter } from "react-native-search-filter";
import { ListItem, Icon } from "react-native-elements";
import { patients } from "../../config/data";
import { titleCase } from "../../config/helpers";
import * as dbHelpers from "../../config/databaseHelpers";
var SQLite = require("react-native-sqlite-storage");
const KEYS_TO_FILTERS = ["nom", "prenom"];

export default class Patients extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      patients: [],
      searchTerm: "",
      error: ""
    };
  }
  goToDetails = patient => {
    this.props.navigation.navigate("PatientDetails", { ...patient });
  };

  executeQuery = (query, parameters) => {
    var db = SQLite.openDatabase(
      {
        name: "nephrosystem.db"
      },
      dbHelpers.openCB,
      dbHelpers.errorOpen
    );
    db.transaction(
      tx => {
        tx.executeSql(query, parameters, (tx, results) => {
          console.log("Query completed");
          var len = results.rows.length;
          let rows = results.rows.raw();
          if (len > 0) this.setState({ patients: rows, loading: false });
          else
            this.setState({
              error: "Aucun patient. Synchronisez svp et réessayez!",
              loading: false
            });
        });
      },
      dbHelpers.successCB,
      () => {
        dbHelpers.errorCB();
      }
    );
  };

  componentDidMount = async () => {
    const user = JSON.parse(await AsyncStorage.getItem("user"));
    await this.executeQuery("select * from patients", []);
  };

  render() {
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color={styles.color} />
        </View>
      );
    } else {
      if (this.state.error != "") {
        return (
          <View style={styles.container}>
            <Text style={{ fontSize: 20, color: styles.color }}>
              {this.state.error}
            </Text>
          </View>
        );
      }
    }
    const filteredPatients = this.state.patients.filter(
      createFilter(this.state.searchTerm, KEYS_TO_FILTERS)
    );
    return (
      <View>
        <SearchInput
          onChangeText={term => {
            this.setState({ searchTerm: term });
          }}
          style={styles.searchInput}
          placeholder="Rechercher un patient"
        />
        <ScrollView>
          <View containerStyle={{ marginTop: 0 }}>
            {filteredPatients.map(patient => (
              <ListItem
                key={patient.codepatient}
                roundAvatar
                chevron
                chevronColor={styles.color}
                leftAvatar={{ source: { uri: patient.photo } }}
                title={`${patient.nom.toUpperCase()} ${titleCase(
                  patient.prenom
                )}`}
                subtitle={patient.sexe}
                onPress={() => this.goToDetails(patient)}
              />
            ))}
          </View>
        </ScrollView>
      </View>
    );
  }
}
