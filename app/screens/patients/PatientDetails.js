import React, { Component } from "react";
import { ScrollView, View } from "react-native";
import { Tile, ListItem, Button } from "react-native-elements";
import { titleCase } from "../../config/helpers";
import localStyles from "./styles";
import styles from "../../config/styles";

class PatientDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isButtonLoading: false
    };
  }
  goToPatientFolder = () => {
    const patient = this.props.navigation.state.params;
    this.setState({ isButtonLoading: false });

    this.props.navigation.navigate("PatientFolder", {
      ...patient
    });
  };
  render() {
    const {
      photo,
      nom,
      prenom,
      datenaissance,
      lieunaissance,
      sexe,
      codepatient
    } = this.props.navigation.state.params;

    return (
      <ScrollView>
        <Tile
          imageSrc={{ uri: photo }}
          featured
          title={`${nom.toUpperCase()} ${prenom.toUpperCase()}`}
          caption={sexe}
        />
        <View style={styles.container}>
          <Button
            onPress={() => this.goToPatientFolder()}
            loading={this.state.isButtonLoading}
            titleStyle={{ fontWeight: "700" }}
            loadingProps={{ size: "large", color: "white" }}
            activityIndicatorStyle={{ backgroundColor: "white" }}
            title="Voir dossier patient"
            buttonStyle={styles.button}
          />
        </View>
        <View style={{ marginTop: 0 }}>
          <ListItem
            title="Code"
            titleStyle={localStyles.title}
            rightTitle={codepatient}
            hideChevron
          />
          <ListItem
            title="Prenom"
            titleStyle={localStyles.title}
            rightTitle={titleCase(prenom)}
            hideChevron
          />
          <ListItem
            title="Nom"
            titleStyle={localStyles.title}
            rightTitle={nom.toUpperCase()}
            hideChevron
          />
        </View>

        <View style={{ marginTop: 10 }}>
          <ListItem
            title="Sexe"
            titleStyle={localStyles.title}
            rightTitle={sexe}
            hideChevron
          />
        </View>

        <View style={{ marginTop: 10 }}>
          <ListItem
            title="Date de naissance"
            titleStyle={localStyles.title}
            rightTitle={datenaissance}
            hideChevron
          />
          <ListItem
            title="Lieu de naissance"
            titleStyle={localStyles.title}
            rightTitle={lieunaissance}
            hideChevron
          />
        </View>
      </ScrollView>
    );
  }
}

export default PatientDetails;
