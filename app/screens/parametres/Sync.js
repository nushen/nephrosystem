import React, { Component } from "react";
import { View, AsyncStorage, Text } from "react-native";
import styles from "../../config/styles";
import { Button, Icon } from "react-native-elements";
import * as globalHelpers from "../../config/helpers";
var SQLite = require("react-native-sqlite-storage");
import * as dbHelpers from "../../config/databaseHelpers";

export default class Sync extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: "",
      patientsFromServer: [],
      rendezVousFromServer: [],
      fichiersFromServer: [],
      fichiersFromDatabase: [],
      syncing: false,
      allGood: true,
      error: ""
    };
  }

  //Query the database
  executeQuery = (query, parameters, success, error) => {
    var db = SQLite.openDatabase(
      {
        name: "nephrosystem.db"
      },
      dbHelpers.openCB,
      dbHelpers.errorOpen
    );
    db.transaction(
      tx => {
        tx.executeSql(query, parameters, (tx, results) => {
          console.log("Query completed");
          var len = results.rows.length;
          let rows = results.rows.raw();
          console.log("Query succesfully executed");
        });
      },
      success,
      error
    );
  };

  /*
  uploadData = async () => {
    //Upload des fichiers non synchronisés vers le serveur
    var db = SQLite.openDatabase(
      {
        name: "nephrosystem.db"
      },
      dbHelpers.openCB,
      dbHelpers.errorOpen
    );
    db.transaction(
      tx => {
        tx.executeSql(
          "select * from fichiers where synced=false",
          [],
          (tx, results) => {
            console.log("Query completed");
            var len = results.rows.length;
            let rows = results.rows.raw();
            console.log(rows);

            if (len > 0) {
              rows.map(async row => {
                await globalHelpers.uploadFichier(row);
              });
            } else
              this.setState({
                error: "Erreur d'uploading",
                loading: false
              });
          }
        );
      },
      dbHelpers.successCB,
      error => {
        dbHelpers.errorCB(error);
      }
    );
  };
  */

  downloadData = async () => {
    const user = JSON.parse(await AsyncStorage.getItem("user"));
    const patients = await globalHelpers.getPatients(user.id);
    const rendezvous = await globalHelpers.getRendezVous(user.id);
    const fichiers = await globalHelpers.getFichiers();

    if (patients && rendezvous) {
      this.setState({
        fichiersFromServer: fichiers,
        patientsFromServer: patients,
        rendezVousFromServer: rendezvous
      });
    } else {
      this.setState({
        message: "Erreur serveur. Réessayez plus tard!",
        allGood: false,
        syncing: false
      });
    }
  };

  //Store data into local database
  sync = async () => {
    console.log("syncing in progress...");
    this.setState({
      message: "Synchronisation en cours...",
      syncing: true
    });
    await this.downloadData();
    //await this.uploadData();
    if (this.state.patientsFromServer.length > 0)
      await SQLite.deleteDatabase(
        {
          name: "nephrosystem.db"
        },
        async () => {
          dbHelpers.successDelete;
          var db = SQLite.openDatabase(
            {
              name: "nephrosystem.db"
            },
            dbHelpers.openCB,
            dbHelpers.errorOpen
          );
          db.transaction(
            tx => {
              //patients table
              tx.executeSql(
                "CREATE TABLE IF NOT EXISTS patients (id, actif, codepatient, prenom, nom, sexe, datenaissance, lieunaissance, photo, nephrologue, synced)",
                [],
                (tx, results) => {
                  console.log("Query completed");
                  var len = results.rows.length;
                  let rows = results.rows.raw();
                  console.log(
                    "Query succesfully executed : patients table creation"
                  );
                }
              );
              if (this.state.patientsFromServer.length > 0)
                this.state.patientsFromServer.map(patient => {
                  tx.executeSql(
                    "insert into patients (id, actif, codepatient, prenom, nom, sexe, datenaissance, lieunaissance, photo, nephrologue, synced) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                    [
                      patient.id,
                      patient.actif,
                      patient.codepatient,
                      patient.prenom,
                      patient.nom,
                      patient.sexe,
                      patient.datenaissance,
                      patient.lieunaissance,
                      patient.photo,
                      patient.nephrologue,
                      true
                    ],
                    (tx, results) => {
                      console.log("Query completed");
                      var len = results.rows.length;
                      let rows = results.rows.raw();
                      console.log(
                        "Query succesfully executed : patients table insertion"
                      );
                    }
                  );
                });
              //rendezvous table
              tx.executeSql(
                "CREATE TABLE IF NOT EXISTS rendezvous (id, title, description, daterv, datedebut, datefin, etat, medecin_id, patient_id, synced)",
                [],
                (tx, results) => {
                  console.log("Query completed");
                  var len = results.rows.length;
                  let rows = results.rows.raw();
                  console.log(
                    "Query succesfully executed : rendezvous table creation"
                  );
                }
              );
              if (this.state.rendezVousFromServer.length > 0)
                this.state.rendezVousFromServer.map(rv => {
                  tx.executeSql(
                    "insert into rendezvous (id, title, description, daterv, datedebut, datefin, etat, medecin_id, patient_id, synced) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                    [
                      rv.id,
                      rv.title,
                      rv.description,
                      rv.daterv,
                      rv.datedebut,
                      rv.datefin,
                      rv.etat,
                      rv.medecin_id,
                      rv.patient_id,
                      true
                    ],
                    (tx, results) => {
                      console.log("Query completed");
                      var len = results.rows.length;
                      let rows = results.rows.raw();
                      console.log(
                        "Query succesfully executed : rendezvous table insertion"
                      );
                    }
                  );
                });
              //fichiers table
              tx.executeSql(
                "CREATE TABLE IF NOT EXISTS fichiers (titre, source, type, patient, synced)",
                [],
                (tx, results) => {
                  console.log("Query completed");
                  var len = results.rows.length;
                  let rows = results.rows.raw();
                  console.log(
                    "Query succesfully executed : fichiers table creation"
                  );
                }
              );

              if (this.state.fichiersFromServer.length > 0)
                this.state.fichiersFromServer.map(fichier => {
                  tx.executeSql(
                    "insert into fichiers (titre, source, type, patient, synced) values (?, ?, ?, ?, ?)",
                    [
                      fichier.titre,
                      fichier.source,
                      fichier.type,
                      fichier.patient,
                      true
                    ],
                    (tx, results) => {
                      console.log("Query completed");
                      var len = results.rows.length;
                      let rows = results.rows.raw();
                      console.log(
                        "Query succesfully executed : fichiers table insertion"
                      );
                    }
                  );
                });
            },
            () => {
              dbHelpers.successCB();
            },
            error => {
              console.log("========= Erreur SQL :" + error + " =========");
              dbHelpers.errorCB(error);
            }
          );
        },
        error => {
          dbHelpers.errorDelete(error);
          console.log("Erreur suppression base de données");

          this.setState({
            message: "Erreur inattendue. Réessayez plus tard!",
            allGood: false,
            syncing: false
          });
        }
      );
    if (this.state.allGood)
      this.setState({
        message: "Synchronisation effectuée",
        syncing: false
      });
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={{ fontSize: 20, color: styles.color }}>
          {this.state.message}
        </Text>
        <Button
          title="Synchroniser"
          icon={<Icon name="sync" size={25} color="white" />}
          buttonStyle={styles.button}
          loading={this.state.syncing}
          loadingProps={{ size: "large", color: "white" }}
          activityIndicatorStyle={{ backgroundColor: "green" }}
          onPress={() => this.sync()}
        />
      </View>
    );
  }
}
