import React from "react";
import { View, Text, ScrollView, AsyncStorage } from "react-native";
import styles from "../../config/styles";
import localStyles from "./styles";
import { List, ListItem } from "react-native-elements";

export default class Parametres extends React.Component {
  goTo = screen => {
    console.log("Screen :" + screen);
  };

  followLink = link => {
    console.log("Lien externe :" + link);
  };

  logout = async () => {
    await AsyncStorage.removeItem("user");
    this.props.navigation.navigate("Login");
  };

  goToSync = () => {
    this.props.navigation.navigate("Sync");
  };

  render() {
    return (
      <ScrollView>
        <View containerStyle={{ marginTop: 10 }}>
          <Text style={localStyles.sectionTitle}>Mes informations</Text>
          <ListItem
            title="Synchronisation"
            chevron
            chevronColor={styles.color}
            leftIcon={{ name: "sync", color: styles.color }}
            onPress={() => this.goToSync()}
          />
          <ListItem
            title="Mon historique"
            chevron
            chevronColor={styles.color}
            leftIcon={{ name: "access-time", color: styles.color }}
            onPress={() => this.goTo("Historique")}
          />
          <ListItem
            title="Changer de mot de passe"
            chevron
            chevronColor={styles.color}
            leftIcon={{ name: "lock", color: styles.color }}
            onPress={() => this.goTo("EditPassword")}
          />
        </View>
        <View containerStyle={{ marginBottom: 10 }}>
          <Text style={localStyles.sectionTitle}>
            À propos de l'application
          </Text>
          <ListItem
            title="Notez l'application"
            chevron
            chevronColor={styles.color}
            leftIcon={{ name: "create", color: styles.color }}
            onPress={() => this.followLink("note")}
          />
          <ListItem
            title="Donnez votre avis"
            chevron
            chevronColor={styles.color}
            leftIcon={{ name: "chat-bubble", color: styles.color }}
            onPress={() => this.followLink("avis")}
          />
          <ListItem
            title="Contactez-nous"
            chevron
            chevronColor={styles.color}
            leftIcon={{ name: "phone", color: styles.color }}
            onPress={() => this.followLink("contact")}
          />
        </View>
        <View containerStyle={{ marginTop: 10 }}>
          <ListItem
            title="Déconnexion"
            leftIcon={{ name: "exit-to-app", color: "red" }}
            hideChevron
            onPress={() => this.logout()}
          />
        </View>
      </ScrollView>
    );
  }
}
