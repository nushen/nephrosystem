import React, { Component } from "react";
import { AsyncStorage } from "react-native";
import { LoginStack } from "./config/routes";
import Splash from "./screens/Splash";

export default class App extends Component {
  constructor() {
    super(),
      (this.state = {
        splash: true
      });
  }

  hideSplash = () => {
    this.setState({ splash: false });
  };

  componentDidMount = async () => {
    setTimeout(() => {
      this.hideSplash();
    }, 3000);
    try {
      const user = JSON.parse(await AsyncStorage.getItem("user"));
      //const user = true;
      if (user) {
        this.nephro = LoginStack(true);
      } else {
        this.nephro = LoginStack(false);
      }
    } catch (error) {
      console.log("+--------------------------------------------+");
      console.log(
        "----------------------------" + error + "----------------------------"
      );
      console.log("+--------------------------------------------+");
    }
  };
  nephro;

  render() {
    return this.state.splash ? <Splash /> : <this.nephro />;
  }
}
