package com.mas.nephro.service;

import java.util.List;
import java.util.Objects;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

import com.mas.nephro.dao.FichiersDao;
import com.mas.nephro.model.Fichier;

public abstract class FichiersService {
	private static final String FICHIER_NOT_FOUND = "Fichier avec l'id %s est introuvable";
	private static final String DATABASE_REACH_ERROR = "Impossible d'accéder à la base de données MySQL. La base de données peut être en panne ou il peut y avoir des problèmes de connectivité du réseau. Détails : ";
	private static final String DATABASE_CONNECTION_ERROR = "Impossible de créer une connexion à la base de données MySQL. La configuration de la base de données est probablement incorrecte. Détails :";
	private static final String DATABASE_UNEXPECTED_ERROR = "Une erreur inattendue s'est produite lors de la tentative d'accès à la base de données. Détails : ";
	private static final String SUCCESS = "Fichier supprimé avec succès";
	private static final String UNEXPECTED_ERROR = "Une erreur inattendue s'est produite lors de la suppression d'un fichier.";

	@CreateSqlObject
	abstract FichiersDao fichiersDao();

	public List<Fichier> getFichiers() {
		return fichiersDao().getFichiers();
	}

	public Fichier getFichier(int id) {
		Fichier fichier = fichiersDao().getFichier(id);
		if (Objects.isNull(fichier)) {
			throw new WebApplicationException(String.format(FICHIER_NOT_FOUND), Status.NOT_FOUND);
		}
		return fichier;
	}

	public Fichier createFichier(Fichier fichier) {
		fichiersDao().createFichier(fichier);
		return fichiersDao().getFichier(fichiersDao().lastInsertId());
	}

	public Fichier editFichier(Fichier fichier) {
		if (Objects.isNull(fichiersDao().getFichier(fichier.getId()))) {
			throw new WebApplicationException(String.format(FICHIER_NOT_FOUND, fichier.getId()), Status.NOT_FOUND);
		}
		fichiersDao().editFichier(fichier);
		return fichiersDao().getFichier(fichier.getId());
	}

	public String deleteFichier(final int id) {
		int result = fichiersDao().deleteFichier(id);
		switch (result) {
		case 1:
			return SUCCESS;
		case 0:
			throw new WebApplicationException(String.format(FICHIER_NOT_FOUND, id), Status.NOT_FOUND);
		default:
			throw new WebApplicationException(UNEXPECTED_ERROR, Status.INTERNAL_SERVER_ERROR);
		}
	}

	public String performHealthCheck() {
		try {
			fichiersDao().getFichiers();
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}
}
