package com.mas.nephro.mapper;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.mas.nephro.model.Patient;

public class PatientsMapper implements ResultSetMapper<Patient> {
	private static final String ID = "id";
	private static final String ACTIF = "actif";
	private static final String CODEPATIENT = "codepatient";
	private static final String PRENOM = "prenom";
	private static final String NOM = "nom";
	private static final String SEXE = "sexe";
	private static final String DATENAISSANCE = "datenaissance";
	private static final String LIEUNAISSANCE = "lieunaissance";
	private static final String PHOTO = "photo";
	private static final String NEPHROLOGUE = "nephrologue";
	
	public Patient map(int index, ResultSet r, StatementContext ctx) throws SQLException {
		// TODO Auto-generated method stub
		return new Patient(r.getInt(ID), r.getBoolean(ACTIF), r.getString(CODEPATIENT), r.getString(PRENOM), r.getString(NOM), r.getString(SEXE), r.getString(DATENAISSANCE), r.getString(LIEUNAISSANCE), r.getString(PHOTO), r.getInt(NEPHROLOGUE));
	}
}
