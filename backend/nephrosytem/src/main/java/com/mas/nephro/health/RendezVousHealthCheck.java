package com.mas.nephro.health;

import com.codahale.metrics.health.HealthCheck;
import com.mas.nephro.service.RendezVousService;

public class RendezVousHealthCheck extends HealthCheck {
	private static final String HEALTHY = "Le service RendezVous de NephroSystem est sain pour la lecture et l'écriture";
	private static final String UNHEALTHY = "Le service RendezVous de NephroSystem n'est pas sain. ";
	private static final String MESSAGE_PLACEHOLDER = "{}";

	private final RendezVousService rendezVousService;

	public RendezVousHealthCheck(RendezVousService rendezVousService) {
		this.rendezVousService = rendezVousService;
	}

	@Override
	protected Result check() throws Exception {
		String mySqlHealthStatus = rendezVousService.performHealthCheck();
		if (mySqlHealthStatus == null) {
			return Result.healthy(HEALTHY);
		} else {
			return Result.unhealthy(UNHEALTHY + MESSAGE_PLACEHOLDER, mySqlHealthStatus);
		}
	}

}
