package com.mas.nephro.health;
import com.codahale.metrics.health.HealthCheck;
import com.mas.nephro.service.PatientsService;

public class PatientsHealthCheck extends HealthCheck {
	private static final String HEALTHY = "Le service Patients de NephroSystem est sain pour la lecture et l'écriture";
	private static final String UNHEALTHY = "Le service Patients de NephroSystem n'est pas sain. ";
	private static final String MESSAGE_PLACEHOLDER = "{}";
	
	private final PatientsService patientsService;
	
	

	public PatientsHealthCheck(PatientsService patientsService) {
		this.patientsService = patientsService;
	}



	@Override
	protected Result check() throws Exception {
		String mySqlHealthStatus = patientsService.performHealthCheck();
	    if (mySqlHealthStatus == null) {
		  return Result.healthy(HEALTHY);
	    } else {
	      return Result.unhealthy(UNHEALTHY + MESSAGE_PLACEHOLDER, mySqlHealthStatus);
	    }
	}

}
