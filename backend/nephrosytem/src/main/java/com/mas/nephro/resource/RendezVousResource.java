package com.mas.nephro.resource;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.jetty.http.HttpStatus;

import com.codahale.metrics.annotation.Timed;
import com.mas.nephro.model.RendezVous;
import com.mas.nephro.core.Representation;
import com.mas.nephro.service.RendezVousService;

@Path("/rendezVous")
@Produces(MediaType.APPLICATION_JSON)
public class RendezVousResource {
	private final RendezVousService rendezVousService;

	public RendezVousResource(RendezVousService rendezVousService) {
		this.rendezVousService = rendezVousService;
	}

	@GET
	@Timed
	public Representation<List<RendezVous>> getRendezVous() {
		return new Representation<List<RendezVous>>(HttpStatus.OK_200, rendezVousService.getRendezVous());
	}

	@GET
	@Timed
	@Path("{id}")
	public Representation<RendezVous> getRendezVous(@PathParam("id") final int id) {
		return new Representation<RendezVous>(HttpStatus.OK_200, rendezVousService.getRendezVous(id));
	}

	@POST
	@Timed
	public Representation<RendezVous> createRendezVous(@NotNull @Valid final RendezVous rendezVous) {
		return new Representation<RendezVous>(HttpStatus.OK_200, rendezVousService.createRendezVous(rendezVous));
	}

	@PUT
	@Timed
	@Path("{id}")
	public Representation<RendezVous> editRendezVous(@NotNull @Valid final RendezVous rendezVous,
			@PathParam("id") final int id) {
		rendezVous.setId(id);
		return new Representation<RendezVous>(HttpStatus.OK_200, rendezVousService.editRendezVous(rendezVous));
	}

	@DELETE
	@Timed
	@Path("{id}")
	public Representation<String> deleteRendezVous(@PathParam("id") final int id) {
		return new Representation<String>(HttpStatus.OK_200, rendezVousService.deleteRendezVous(id));
	}
}
