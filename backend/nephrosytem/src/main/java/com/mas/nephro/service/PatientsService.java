package com.mas.nephro.service;

import java.util.List;
import java.util.Objects;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

import com.mas.nephro.dao.PatientsDao;
import com.mas.nephro.model.Patient;
import com.mas.nephro.dao.FichiersDao;
import com.mas.nephro.model.Fichier;

public abstract class PatientsService {
	private static final String PATIENT_NOT_FOUND = "Patient avec l'id %s est introuvable";
	private static final String DATABASE_REACH_ERROR = "Impossible d'accéder à la base de données MySQL. La base de données peut être en panne ou il peut y avoir des problèmes de connectivité du réseau. Détails : ";
	private static final String DATABASE_CONNECTION_ERROR = "Impossible de créer une connexion à la base de données MySQL. La configuration de la base de données est probablement incorrecte. Détails :";
	private static final String DATABASE_UNEXPECTED_ERROR = "Une erreur inattendue s'est produite lors de la tentative d'accès à la base de données. Détails : ";
	private static final String SUCCESS = "Patient supprimé avec succès";
	private static final String UNEXPECTED_ERROR = "Une erreur inattendue s'est produite lors de la suppression d'un patient.";

	@CreateSqlObject
	abstract PatientsDao patientsDao();

	@CreateSqlObject
	abstract FichiersDao fichiersDao();

	public List<Patient> getPatients() {
		return patientsDao().getPatients();
	}

	public Patient getPatient(int id) {
		Patient patient = patientsDao().getPatient(id);
		if (Objects.isNull(patient)) {
			throw new WebApplicationException(String.format(PATIENT_NOT_FOUND), Status.NOT_FOUND);
		}
		return patient;
	}
	
	public Integer getNombreFichiersPatient(int id) {
		return fichiersDao().getNombreFichiersPatient(id);
	}

	public List<Fichier> getFichiersPatient(int id) {
		return fichiersDao().getFichiersPatient(id);
	}

	public Patient createPatient(Patient patient) {
		patientsDao().createPatient(patient);
		return patientsDao().getPatient(patientsDao().lastInsertId());
	}

	public Patient editPatient(Patient patient) {
		if (Objects.isNull(patientsDao().getPatient(patient.getId()))) {
			throw new WebApplicationException(String.format(PATIENT_NOT_FOUND, patient.getId()), Status.NOT_FOUND);
		}
		patientsDao().editPatient(patient);
		return patientsDao().getPatient(patient.getId());
	}

	public String deletePatient(final int id) {
		int result = patientsDao().deletePatient(id);
		switch (result) {
		case 1:
			return SUCCESS;
		case 0:
			throw new WebApplicationException(String.format(PATIENT_NOT_FOUND, id), Status.NOT_FOUND);
		default:
			throw new WebApplicationException(UNEXPECTED_ERROR, Status.INTERNAL_SERVER_ERROR);
		}
	}

	public String performHealthCheck() {
		try {
			patientsDao().getPatients();
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}
}
