package com.mas.nephro.dao;

import java.util.List;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import com.mas.nephro.mapper.PatientsMapper;
import com.mas.nephro.model.Patient;

@RegisterMapper(PatientsMapper.class)
public interface PatientsDao {
	@SqlQuery("select * from patients;")
	public List<Patient> getPatients();
	
	@SqlQuery("select * from patients where nephrologue=:id")
	public List<Patient> getPatientsUser(@Bind("id") final int id);

	@SqlQuery("select * from patients where id=:id")
	public Patient getPatient(@Bind("id") final int id);

	@SqlUpdate("insert into patients(actif, codepatient, prenom, nom, sexe, datenaissance, lieunaissance, photo, nephrologue) values(:actif, :codepatient, :prenom, :nom, :sexe, :datenaissance, :lieunaissance, :photo, :nephrologue)")
	void createPatient(@BindBean final Patient patient);

	@SqlUpdate("update patients set actif = coalesce(:actif, actif), codepatient = coalesce(:codepatient, codepatient), prenom = coalesce(:prenom, prenom), nom = coalesce(:nom, nom), sexe = coalesce(:sexe, sexe), datenaissance = coalesce(:datenaissance, datenaissance), lieunaissance = coalesce(:lieunaissance, lieunaissance), photo = coalesce(:photo, photo), nephrologue = coalesce(:nephrologue, nephrologue) where id=:id")
	void editPatient(@BindBean final Patient patient);

	@SqlUpdate("delete from patients where id = :id")
	int deletePatient(@Bind("id") final int id);

	@SqlQuery("select last_insert_id();")
	public int lastInsertId();
}
