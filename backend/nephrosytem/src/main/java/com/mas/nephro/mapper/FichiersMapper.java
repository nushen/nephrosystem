package com.mas.nephro.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.mas.nephro.model.Fichier;

public class FichiersMapper implements ResultSetMapper<Fichier> {
	private static final String ID = "id";
	private static final String TITRE = "titre";
	private static final String SOURCE = "source";
	private static final String TYPE = "type";
	private static final String PATIENT = "patient";

	public Fichier map(int index, ResultSet r, StatementContext ctx) throws SQLException {
		// TODO Auto-generated method stub
		return new Fichier(r.getInt(ID), r.getString(TITRE), r.getString(SOURCE), r.getString(TYPE), r.getInt(PATIENT));
	}
}
