package com.mas.nephro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "rendezvous")
public class RendezVous {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "title")
	private String title;

	@Column(name = "description")
	private String description;

	@Column(name = "daterv")
	private String daterv;

	@Column(name = "datedebut")
	private String datedebut;

	@Column(name = "datefin")
	private String datefin;

	@Column(name = "etat")
	private String etat;

	@Column(name = "medecin_id")
	private int medecin_id;

	@Column(name = "patient_id")
	private int patient_id;

	public RendezVous() {
		super();
	}

	public RendezVous(int id, String title, String description, String daterv, String datedebut, String datefin,
			String etat, int medecin_id, int patient_id) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.daterv = daterv;
		this.datedebut = datedebut;
		this.datefin = datefin;
		this.etat = etat;
		this.medecin_id = medecin_id;
		this.patient_id = patient_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDaterv() {
		return daterv;
	}

	public void setDaterv(String daterv) {
		this.daterv = daterv;
	}

	public String getDatedebut() {
		return datedebut;
	}

	public void setDatedebut(String datedebut) {
		this.datedebut = datedebut;
	}

	public String getDatefin() {
		return datefin;
	}

	public void setDatefin(String datefin) {
		this.datefin = datefin;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public int getMedecin_id() {
		return medecin_id;
	}

	public void setMedecin_id(int medecin_id) {
		this.medecin_id = medecin_id;
	}

	public int getPatient_id() {
		return patient_id;
	}

	public void setPatient_id(int patient_id) {
		this.patient_id = patient_id;
	}
	
	
}
