package com.mas.nephro.resource;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.jetty.http.HttpStatus;

import com.codahale.metrics.annotation.Timed;
import com.mas.nephro.model.Patient;
import com.mas.nephro.model.Fichier;
import com.mas.nephro.core.Representation;
import com.mas.nephro.service.PatientsService;

@Path("/patients")
@Produces(MediaType.APPLICATION_JSON)
public class PatientsResource {
	private final PatientsService patientsService;

	public PatientsResource(PatientsService patientsService) {
		this.patientsService = patientsService;
	}

	@GET
	@Timed
	public Representation<List<Patient>> getPatients() {
		return new Representation<List<Patient>>(HttpStatus.OK_200, patientsService.getPatients());
	}

	@GET
	@Timed
	@Path("{id}")
	public Representation<Patient> getPatient(@PathParam("id") final int id) {
		return new Representation<Patient>(HttpStatus.OK_200, patientsService.getPatient(id));
	}
	
	@GET
	@Timed
	@Path("{id}/fichiers/count")
	public Representation<Integer> getNombreFichiersPatient(@PathParam("id") final int id) {
		return new Representation<Integer>(HttpStatus.OK_200, patientsService.getNombreFichiersPatient(id));
	}

	@GET
	@Timed
	@Path("{id}/fichiers")
	public Representation<List<Fichier>> getFichiersPatient(@PathParam("id") final int id) {
		return new Representation<List<Fichier>>(HttpStatus.OK_200, patientsService.getFichiersPatient(id));
	}

	@POST
	@Timed
	public Representation<Patient> createPatient(@NotNull @Valid final Patient patient) {
		return new Representation<Patient>(HttpStatus.OK_200, patientsService.createPatient(patient));
	}

	@PUT
	@Timed
	@Path("{id}")
	public Representation<Patient> editPatient(@NotNull @Valid final Patient patient, @PathParam("id") final int id) {
		patient.setId(id);
		return new Representation<Patient>(HttpStatus.OK_200, patientsService.editPatient(patient));
	}

	@DELETE
	@Timed
	@Path("{id}")
	public Representation<String> deletePatient(@PathParam("id") final int id) {
		return new Representation<String>(HttpStatus.OK_200, patientsService.deletePatient(id));
	}
}
