package com.mas.nephro.resource;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.jetty.http.HttpStatus;

import com.codahale.metrics.annotation.Timed;
import com.mas.nephro.model.Fichier;
import com.mas.nephro.core.Representation;
import com.mas.nephro.service.FichiersService;

@Path("/fichiers")
@Produces(MediaType.APPLICATION_JSON)
public class FichiersResource {
	private final FichiersService fichiersService;

	public FichiersResource(FichiersService fichiersService) {
		this.fichiersService = fichiersService;
	}

	@GET
	@Timed
	public Representation<List<Fichier>> getFichiers() {
		return new Representation<List<Fichier>>(HttpStatus.OK_200, fichiersService.getFichiers());
	}

	@GET
	@Timed
	@Path("{id}")
	public Representation<Fichier> getFichier(@PathParam("id") final int id) {
		return new Representation<Fichier>(HttpStatus.OK_200, fichiersService.getFichier(id));
	}

	@POST
	@Timed
	public Representation<Fichier> createFichier(@NotNull @Valid final Fichier fichier) {
		return new Representation<Fichier>(HttpStatus.OK_200, fichiersService.createFichier(fichier));
	}

	@PUT
	@Timed
	@Path("{id}")
	public Representation<Fichier> editFichier(@NotNull @Valid final Fichier fichier, @PathParam("id") final int id) {
		fichier.setId(id);
		return new Representation<Fichier>(HttpStatus.OK_200, fichiersService.editFichier(fichier));
	}

	@DELETE
	@Timed
	@Path("{id}")
	public Representation<String> deleteFichier(@PathParam("id") final int id) {
		return new Representation<String>(HttpStatus.OK_200, fichiersService.deleteFichier(id));
	}
}
