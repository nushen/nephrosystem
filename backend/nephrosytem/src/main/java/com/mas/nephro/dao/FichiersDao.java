package com.mas.nephro.dao;

import java.util.List;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import com.mas.nephro.mapper.FichiersMapper;
import com.mas.nephro.model.Fichier;

@RegisterMapper(FichiersMapper.class)
public interface FichiersDao {
	@SqlQuery("select * from fichiers;")
	public List<Fichier> getFichiers();

	@SqlQuery("select * from fichiers where patient=:id")
	public List<Fichier> getFichiersPatient(@Bind("id") final int id);

	@SqlQuery("select * from fichiers where id=:id")
	public Fichier getFichier(@Bind("id") final int id);

	@SqlUpdate("insert into fichiers(titre, source, type, patient) values(:titre, :source, :type, :patient)")
	void createFichier(@BindBean final Fichier fichier);

	@SqlUpdate("update fichiers set titre = coalesce(:titre, titre), source = coalesce(:source, source), type = coalesce(:type, type), patient = coalesce(:patient, patient) where id=:id")
	void editFichier(@BindBean final Fichier fichier);

	@SqlUpdate("delete from fichiers where id = :id")
	int deleteFichier(@Bind("id") final int id);

	@SqlQuery("select last_insert_id();")
	public int lastInsertId();

	@SqlQuery("select count(*) from fichiers where patient=:id")
	public Integer getNombreFichiersPatient(@Bind("id") final int id);
}
