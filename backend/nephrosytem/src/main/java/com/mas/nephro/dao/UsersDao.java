package com.mas.nephro.dao;

import java.util.List;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import com.mas.nephro.mapper.UsersMapper;
import com.mas.nephro.model.User;

@RegisterMapper(UsersMapper.class)
public interface UsersDao {
	@SqlQuery("select * from user;")
	public List<User> getUsers();

	@SqlQuery("select * from user where id=:id")
	public User getUser(@Bind("id") final int id);

	@SqlUpdate("insert into user(username, hash, salt, email_address, first_name, last_name, ce_service, is_active, is_super_admin, last_login, created_at, updated_at) values(:username, :hash, :salt, :email_address, :first_name, :last_name, :ce_service, :is_active, :is_super_admin, :last_login, :created_at, :updated_at)")
	void createUser(@BindBean final User user);

	@SqlUpdate("update user set username = coalesce(:username, username), hash = coalesce(:hash, hash), salt = coalesce(:salt, salt), email_address = coalesce(:email_address, email_address), first_name = coalesce(:first_name, first_name), last_name = coalesce(:last_name, last_name), ce_service = coalesce(:ce_service, ce_service), is_active = coalesce(:is_active, is_active), is_super_admin = coalesce(:is_super_admin, is_super_admin), last_login = coalesce(:last_login, last_login), created_at = coalesce(:created_at, created_at), updated_at = coalesce(:updated_at, updated_at) where id=:id")
	void editUser(@BindBean final User user);
	
	@SqlUpdate("delete from user where id = :id")
	int deleteUser(@Bind("id") final int id);
	
	@SqlQuery("select last_insert_id();")
	public int lastInsertId();
}
