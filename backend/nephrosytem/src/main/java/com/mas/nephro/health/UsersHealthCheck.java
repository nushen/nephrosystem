package com.mas.nephro.health;
import com.codahale.metrics.health.HealthCheck;
import com.mas.nephro.service.UsersService;

public class UsersHealthCheck extends HealthCheck {
	private static final String HEALTHY = "Le service de NephroSystem est sain pour la lecture et l'écriture";
	private static final String UNHEALTHY = "Le service de NephroSystem n'est pas sain. ";
	private static final String MESSAGE_PLACEHOLDER = "{}";
	
	private final UsersService usersService;
	
	

	public UsersHealthCheck(UsersService usersService) {
		this.usersService = usersService;
	}



	@Override
	protected Result check() throws Exception {
		String mySqlHealthStatus = usersService.performHealthCheck();
	    if (mySqlHealthStatus == null) {
		  return Result.healthy(HEALTHY);
	    } else {
	      return Result.unhealthy(UNHEALTHY + MESSAGE_PLACEHOLDER, mySqlHealthStatus);
	    }
	}

}
