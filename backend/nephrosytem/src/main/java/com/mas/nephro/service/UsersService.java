package com.mas.nephro.service;
import java.util.List;
import java.util.Objects;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;


import com.mas.nephro.dao.UsersDao;
import com.mas.nephro.dao.PatientsDao;
import com.mas.nephro.dao.RendezVousDao;
import com.mas.nephro.model.User;
import com.mas.nephro.model.Patient;
import com.mas.nephro.model.RendezVous;

public abstract class UsersService {
	private static final String USER_NOT_FOUND = "Utilisateur avec l'id %s est introuvable";
	  private static final String DATABASE_REACH_ERROR =
	      "Impossible d'accéder à la base de données MySQL. La base de données peut être en panne ou il peut y avoir des problèmes de connectivité du réseau. Détails : ";
	  private static final String DATABASE_CONNECTION_ERROR =
	      "Impossible de créer une connexion à la base de données MySQL. La configuration de la base de données est probablement incorrecte. Détails :";
	  private static final String DATABASE_UNEXPECTED_ERROR =
	      "Une erreur inattendue s'est produite lors de la tentative d'accès à la base de données. Détails : ";
	  private static final String SUCCESS = "Utilisateur supprimé avec succès";
	  private static final String UNEXPECTED_ERROR = "Une erreur inattendue s'est produite lors de la suppression d'un utilisateur.";
	  
	  @CreateSqlObject
	  abstract UsersDao usersDao();
	  
	  @CreateSqlObject
	  abstract PatientsDao patientsDao();
	  
	  @CreateSqlObject
	  abstract RendezVousDao rendezVousDao();
	  
	  public List<User> getUsers() {
		  return usersDao().getUsers();
	  }
	  
	  public User getUser(int id) {
		  User user = usersDao().getUser(id);
		  if(Objects.isNull(user)) {
			  throw new WebApplicationException(String.format(USER_NOT_FOUND), Status.NOT_FOUND);
		  }
		  return user;
	  }
	  
		public List<Patient> getPatients(int id){
			return patientsDao().getPatientsUser(id);
		}
		
		public List<RendezVous> getRendezVous(int id){
			return rendezVousDao().getRendezVousUser(id);
		}
	  
	  public User createUser(User user) {
		  usersDao().createUser(user);
		  return usersDao().getUser(usersDao().lastInsertId());
	  }
	  
	  public User editUser(User user) {
		  if(Objects.isNull(usersDao().getUser(user.getId()))) {
			  throw new WebApplicationException(String.format(USER_NOT_FOUND, user.getId()), Status.NOT_FOUND);
		  }
		  usersDao().editUser(user);
		  return usersDao().getUser(user.getId());
	  }
	  
	  public String deleteUser(final int id) {
		  int result = usersDao().deleteUser(id);
		  switch(result) {
		  case 1:
			  return SUCCESS;
		  case 0:
			  throw new WebApplicationException(String.format(USER_NOT_FOUND, id), Status.NOT_FOUND);
		  default:
			  throw new WebApplicationException(UNEXPECTED_ERROR, Status.INTERNAL_SERVER_ERROR);
		  }
	  }
	  
	  public String performHealthCheck() {
	    try {
	      usersDao().getUsers();
	    } catch (UnableToObtainConnectionException ex) {
	      return checkUnableToObtainConnectionException(ex);
	    } catch (UnableToExecuteStatementException ex) {
	      return checkUnableToExecuteStatementException(ex);
	    } catch (Exception ex) {
	      return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
	    }
	    return null;
	  }
	  
	  private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
	    if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
	      return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
	    } else if (ex.getCause() instanceof java.sql.SQLException) {
	      return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
	    } else {
	      return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
	    }
	  }

	  private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
	    if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
	      return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
	    } else {
	      return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
	    }
	  }	  
}
