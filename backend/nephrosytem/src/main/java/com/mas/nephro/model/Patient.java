package com.mas.nephro.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="patient")
public class Patient {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name="actif")
	private boolean actif;
	
	@Column(name="codepatient")
	private String codepatient;
	
	@Column(name="prenom")
	private String prenom;
	
	@Column(name="nom")
	private String nom;
	
	@Column(name="sexe")
	private String sexe;
	
	@Column(name="datenaissance")
	private String datenaissance;
	
	@Column(name="lieunaissance")
	private String lieunaissance;
	
	@Column(name="photo")
	private String photo;
	
	@Column(name="nephrologue")
	private int nephrologue;

	public Patient(int id, boolean actif, String codepatient, String prenom, String nom, String sexe,
			String datenaissance, String lieunaissance, String photo, int nephrologue) {
		super();
		this.id = id;
		this.actif = actif;
		this.codepatient = codepatient;
		this.prenom = prenom;
		this.nom = nom;
		this.sexe = sexe;
		this.datenaissance = datenaissance;
		this.lieunaissance = lieunaissance;
		this.photo = photo;
		this.nephrologue = nephrologue;
	}

	public Patient() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isActif() {
		return actif;
	}

	public void setActif(boolean actif) {
		this.actif = actif;
	}

	public String getCodepatient() {
		return codepatient;
	}

	public void setCodepatient(String codepatient) {
		this.codepatient = codepatient;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public String getDatenaissance() {
		return datenaissance;
	}

	public void setDatenaissance(String datenaissance) {
		this.datenaissance = datenaissance;
	}

	public String getLieunaissance() {
		return lieunaissance;
	}

	public void setLieunaissance(String lieunaissance) {
		this.lieunaissance = lieunaissance;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public int getNephrologue() {
		return nephrologue;
	}

	public void setNephrologue(int nephrologue) {
		this.nephrologue = nephrologue;
	}
}
