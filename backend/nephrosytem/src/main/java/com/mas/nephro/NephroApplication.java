package com.mas.nephro;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

import javax.sql.DataSource;

import org.skife.jdbi.v2.DBI;

import com.mas.nephro.NephroConfiguration;
import com.mas.nephro.health.*;
import com.mas.nephro.resource.*;
import com.mas.nephro.service.*;

public class NephroApplication extends Application<NephroConfiguration> {
	private static final String SQL = "sql";
	private static final String PATIENTS_SERVICE = "Patients Service";
	private static final String USERS_SERVICE = "Users Service";
	private static final String RENDEZVOUS_SERVICE = "Rendez-vous Service";
	private static final String FICHIERS_SERVICE = "Fichiers Service";

	public static void main(String[] args) throws Exception {
		new NephroApplication().run(args);
	}

	@Override
	public void run(NephroConfiguration configuration, Environment environment) throws Exception {
		// TODO Auto-generated method stub
		// Datasource config
		final DataSource dataSource = configuration.getDataSourceFactory().build(environment.metrics(), SQL);
		DBI dbi = new DBI(dataSource);

		// Register Health Checks
		UsersHealthCheck usersHealthCheck = new UsersHealthCheck(dbi.onDemand(UsersService.class));
		environment.healthChecks().register(USERS_SERVICE, usersHealthCheck);

		PatientsHealthCheck patientsHealthCheck = new PatientsHealthCheck(dbi.onDemand(PatientsService.class));
		environment.healthChecks().register(PATIENTS_SERVICE, patientsHealthCheck);

		RendezVousHealthCheck rendezVousHealthCheck = new RendezVousHealthCheck(dbi.onDemand(RendezVousService.class));
		environment.healthChecks().register(RENDEZVOUS_SERVICE, rendezVousHealthCheck);

		FichiersHealthCheck fichiersHealthCheck = new FichiersHealthCheck(dbi.onDemand(FichiersService.class));
		environment.healthChecks().register(FICHIERS_SERVICE, fichiersHealthCheck);

		// Register resources
		environment.jersey().register(new UsersResource(dbi.onDemand(UsersService.class)));
		environment.jersey().register(new PatientsResource(dbi.onDemand(PatientsService.class)));
		environment.jersey().register(new RendezVousResource(dbi.onDemand(RendezVousService.class)));
		environment.jersey().register(new FichiersResource(dbi.onDemand(FichiersService.class)));
	}

}
