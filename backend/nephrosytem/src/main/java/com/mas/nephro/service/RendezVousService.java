package com.mas.nephro.service;

import java.util.List;
import java.util.Objects;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import org.skife.jdbi.v2.exceptions.UnableToExecuteStatementException;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

import com.mas.nephro.dao.RendezVousDao;
import com.mas.nephro.model.RendezVous;

public abstract class RendezVousService {
	private static final String RENDEZVOUS_NOT_FOUND = "Rendez-vous avec l'id %s est introuvable";
	private static final String DATABASE_REACH_ERROR = "Impossible d'accéder à la base de données MySQL. La base de données peut être en panne ou il peut y avoir des problèmes de connectivité du réseau. Détails : ";
	private static final String DATABASE_CONNECTION_ERROR = "Impossible de créer une connexion à la base de données MySQL. La configuration de la base de données est probablement incorrecte. Détails :";
	private static final String DATABASE_UNEXPECTED_ERROR = "Une erreur inattendue s'est produite lors de la tentative d'accès à la base de données. Détails : ";
	private static final String SUCCESS = "Rendez-vous supprimé avec succès";
	private static final String UNEXPECTED_ERROR = "Une erreur inattendue s'est produite lors de la suppression d'un rendez-vous.";

	@CreateSqlObject
	abstract RendezVousDao rendezVousDao();

	public List<RendezVous> getRendezVous() {
		return rendezVousDao().getRendezVous();
	}

	public RendezVous getRendezVous(int id) {
		RendezVous rendezVous = rendezVousDao().getRendezVous(id);
		if (Objects.isNull(rendezVous)) {
			throw new WebApplicationException(String.format(RENDEZVOUS_NOT_FOUND), Status.NOT_FOUND);
		}
		return rendezVous;
	}

	public RendezVous createRendezVous(RendezVous rendezVous) {
		rendezVousDao().createRendezVous(rendezVous);
		return rendezVousDao().getRendezVous(rendezVousDao().lastInsertId());
	}

	public RendezVous editRendezVous(RendezVous rendezVous) {
		if (Objects.isNull(rendezVousDao().getRendezVous(rendezVous.getId()))) {
			throw new WebApplicationException(String.format(RENDEZVOUS_NOT_FOUND, rendezVous.getId()), Status.NOT_FOUND);
		}
		rendezVousDao().editRendezVous(rendezVous);
		return rendezVousDao().getRendezVous(rendezVous.getId());
	}

	public String deleteRendezVous(final int id) {
		int result = rendezVousDao().deleteRendezVous(id);
		switch (result) {
		case 1:
			return SUCCESS;
		case 0:
			throw new WebApplicationException(String.format(RENDEZVOUS_NOT_FOUND, id), Status.NOT_FOUND);
		default:
			throw new WebApplicationException(UNEXPECTED_ERROR, Status.INTERNAL_SERVER_ERROR);
		}
	}

	public String performHealthCheck() {
		try {
			rendezVousDao().getRendezVous();
		} catch (UnableToObtainConnectionException ex) {
			return checkUnableToObtainConnectionException(ex);
		} catch (UnableToExecuteStatementException ex) {
			return checkUnableToExecuteStatementException(ex);
		} catch (Exception ex) {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
		return null;
	}

	private String checkUnableToObtainConnectionException(UnableToObtainConnectionException ex) {
		if (ex.getCause() instanceof java.sql.SQLNonTransientConnectionException) {
			return DATABASE_REACH_ERROR + ex.getCause().getLocalizedMessage();
		} else if (ex.getCause() instanceof java.sql.SQLException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}

	private String checkUnableToExecuteStatementException(UnableToExecuteStatementException ex) {
		if (ex.getCause() instanceof java.sql.SQLSyntaxErrorException) {
			return DATABASE_CONNECTION_ERROR + ex.getCause().getLocalizedMessage();
		} else {
			return DATABASE_UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
		}
	}
}
