package com.mas.nephro.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.mas.nephro.model.RendezVous;

public class RendezVousMapper implements ResultSetMapper<RendezVous> {
	private static final String ID = "id";
	private static final String TITLE = "title";
	private static final String DESCRIPTION = "description";
	private static final String DATERV = "daterv";
	private static final String DATEDEBUT = "datedebut";
	private static final String DATEFIN = "datefin";
	private static final String ETAT = "etat";
	private static final String MEDECIN_ID = "medecin_id";
	private static final String PATIENT_ID = "patient_id";

	public RendezVous map(int index, ResultSet r, StatementContext ctx) throws SQLException {
		// TODO Auto-generated method stub
		return new RendezVous(r.getInt(ID), r.getString(TITLE), r.getString(DESCRIPTION), r.getString(DATERV),
				r.getString(DATEDEBUT), r.getString(DATEFIN), r.getString(ETAT), r.getInt(MEDECIN_ID),
				r.getInt(PATIENT_ID));
	}
}