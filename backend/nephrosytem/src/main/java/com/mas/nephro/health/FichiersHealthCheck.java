package com.mas.nephro.health;

import com.codahale.metrics.health.HealthCheck;
import com.mas.nephro.service.FichiersService;

public class FichiersHealthCheck extends HealthCheck {
	private static final String HEALTHY = "Le service Fichiers de NephroSystem est sain pour la lecture et l'écriture";
	private static final String UNHEALTHY = "Le service Fichiers de NephroSystem n'est pas sain. ";
	private static final String MESSAGE_PLACEHOLDER = "{}";

	private final FichiersService fichiersService;

	public FichiersHealthCheck(FichiersService fichiersService) {
		this.fichiersService = fichiersService;
	}

	@Override
	protected Result check() throws Exception {
		String mySqlHealthStatus = fichiersService.performHealthCheck();
		if (mySqlHealthStatus == null) {
			return Result.healthy(HEALTHY);
		} else {
			return Result.unhealthy(UNHEALTHY + MESSAGE_PLACEHOLDER, mySqlHealthStatus);
		}
	}

}
