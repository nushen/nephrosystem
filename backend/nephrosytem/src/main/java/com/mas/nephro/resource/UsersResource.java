package com.mas.nephro.resource;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.jetty.http.HttpStatus;

import com.codahale.metrics.annotation.Timed;
import com.mas.nephro.model.User;
import com.mas.nephro.model.Patient;
import com.mas.nephro.model.RendezVous;
import com.mas.nephro.core.Representation;
import com.mas.nephro.service.UsersService;

@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
public class UsersResource {
	private final UsersService usersService;

	public UsersResource(UsersService usersService) {
		this.usersService = usersService;
	}
	
	@GET
	@Timed
	public Representation<List<User>> getUsers() {
		return new Representation<List<User>>(HttpStatus.OK_200, usersService.getUsers());
	}
	
	@GET
	@Timed
	@Path("{id}")
	public Representation<User> getUser(@PathParam("id") final int id) {
		return new Representation<User>(HttpStatus.OK_200, usersService.getUser(id));
	}
	
	@GET
	@Timed
	@Path("{id}/patients")
	public Representation<List<Patient>> getPatientsUser(@PathParam("id") final int id) {
		return new Representation<List<Patient>>(HttpStatus.OK_200, usersService.getPatients(id));
	}
	
	@GET
	@Timed
	@Path("{id}/rendezVous")
	public Representation<List<RendezVous>> getRendezVousUser(@PathParam("id") final int id) {
		return new Representation<List<RendezVous>>(HttpStatus.OK_200, usersService.getRendezVous(id));
	}
	
	@POST
	@Timed
	public Representation<User> createUser(@NotNull @Valid final User user) {
		return new Representation<User>(HttpStatus.OK_200, usersService.createUser(user));
	}
	
	@PUT
	@Timed
	@Path("{id}")
	public Representation<User> editUser(@NotNull @Valid final User user, @PathParam("id") final int id) {
		user.setId(id);
		return new Representation<User>(HttpStatus.OK_200, usersService.editUser(user));
	}
	
	@DELETE
	@Timed
	@Path("{id}")
	public Representation<String> deleteUser(@PathParam("id") final int id) {
		return new Representation<String>(HttpStatus.OK_200, usersService.deleteUser(id));
	}
}
