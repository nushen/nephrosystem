package com.mas.nephro.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name="username")
	private String username;
	
	@Column(name="hash")
	private String hash;
	
	@Column(name="salt")
	private String salt;
	
	@Column(name="email_address")
	private String email_address;
	
	@Column(name="first_name")
	private String first_name;
	
	@Column(name="last_name")
	private String last_name;
	
	@Column(name="ce_service")
	private String ce_service;
	
	@Column(name="is_active")
	private boolean is_active;
	
	@Column(name="is_super_admin")
	private boolean is_super_admin;
	
	@Column(name="last_login")
	private String last_login;
	
	@Column(name="created_at")
	private String created_at;
	
	@Column(name="updated_at")
	private String updated_at;
	
	public User() {
		super();
	}

	public User(int id, String username, String hash, String salt, String email_address, String first_name,
			String last_name, String ce_service, boolean is_active, boolean is_super_admin, String last_login,
			String created_at, String updated_at) {
		super();
		this.id = id;
		this.username = username;
		this.hash = hash;
		this.salt = salt;
		this.email_address = email_address;
		this.first_name = first_name;
		this.last_name = last_name;
		this.ce_service = ce_service;
		this.is_active = is_active;
		this.is_super_admin = is_super_admin;
		this.last_login = last_login;
		this.created_at = created_at;
		this.updated_at = updated_at;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getEmail_address() {
		return email_address;
	}

	public void setEmail_address(String email_address) {
		this.email_address = email_address;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getCe_service() {
		return ce_service;
	}

	public void setCe_service(String ce_service) {
		this.ce_service = ce_service;
	}

	public boolean isIs_active() {
		return is_active;
	}

	public void setIs_active(boolean is_active) {
		this.is_active = is_active;
	}

	public boolean isIs_super_admin() {
		return is_super_admin;
	}

	public void setIs_super_admin(boolean is_super_admin) {
		this.is_super_admin = is_super_admin;
	}

	public String getLast_login() {
		return last_login;
	}

	public void setLast_login(String last_login) {
		this.last_login = last_login;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

}
