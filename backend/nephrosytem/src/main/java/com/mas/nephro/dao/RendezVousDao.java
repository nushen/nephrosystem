package com.mas.nephro.dao;

import java.util.List;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import com.mas.nephro.mapper.RendezVousMapper;
import com.mas.nephro.model.RendezVous;

@RegisterMapper(RendezVousMapper.class)
public interface RendezVousDao {
	@SqlQuery("select * from rendezvous;")
	public List<RendezVous> getRendezVous();
	
	@SqlQuery("select * from rendezvous where medecin_id=:id and etat='undone'")
	public List<RendezVous> getRendezVousUser(@Bind("id") final int id);

	@SqlQuery("select * from rendezvous where id=:id")
	public RendezVous getRendezVous(@Bind("id") final int id);

	@SqlUpdate("insert into rendezvous(title, description, daterv, datedebut, datefin, etat, medecin_id, patient_id) values(:title, :description, :daterv, :datedebut, :datefin, :etat, :medecin_id, :patient_id)")
	void createRendezVous(@BindBean final RendezVous rendezVous);

	@SqlUpdate("update rendezvous set title = coalesce(:title, title), description = coalesce(:description, description), daterv = coalesce(:daterv, daterv), datedebut = coalesce(:datedebut, datedebut), datefin = coalesce(:datefin, datefin), etat = coalesce(:etat, etat), medecin_id = coalesce(:medecin_id, medecin_id), patient_id = coalesce(:patient_id, patient_id) where id=:id")
	void editRendezVous(@BindBean final RendezVous rendezVous);

	@SqlUpdate("delete from rendezvous where id = :id")
	int deleteRendezVous(@Bind("id") final int id);

	@SqlQuery("select last_insert_id();")
	public int lastInsertId();
}
