package com.mas.nephro.mapper;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.mas.nephro.model.User;

public class UsersMapper implements ResultSetMapper<User> {
	private static final String ID = "id";
	private static final String USERNAME = "username";
	private static final String HASH = "hash";
	private static final String SALT = "salt";
	private static final String EMAIL_ADDRESS = "email_address";
	private static final String FIRST_NAME = "first_name";
	private static final String LAST_NAME = "last_name";
	private static final String CE_SERVICE = "ce_service";
	private static final String IS_ACTIVE = "is_active";
	private static final String IS_SUPER_ADMIN = "is_super_admin";
	private static final String LAST_LOGIN = "last_login";
	private static final String CREATED_AT = "created_at";
	private static final String UPDATED_AT = "updated_at";
	
	public User map(int index, ResultSet r, StatementContext ctx) throws SQLException {
		// TODO Auto-generated method stub
		return new User(r.getInt(ID), r.getString(USERNAME), r.getString(HASH), r.getString(SALT), r.getString(EMAIL_ADDRESS), r.getString(FIRST_NAME), r.getString(LAST_NAME), r.getString(CE_SERVICE), r.getBoolean(IS_ACTIVE), r.getBoolean(IS_SUPER_ADMIN), r.getString(LAST_LOGIN), r.getString(CREATED_AT), r.getString(UPDATED_AT));
	}
}
